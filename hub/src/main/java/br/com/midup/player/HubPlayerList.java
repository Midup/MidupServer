package br.com.midup.player;

import br.com.midup.common.player.BasePlayerList;
import br.com.midup.data.HubPlayerData;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class HubPlayerList extends BasePlayerList<HubPlayer, HubPlayerData, HubRole> {

    @Override
    public HubRole getSpecificRole(int id) {
        return HubRole.getRole(id);
    }

    @Override
    public Set<HubRole> getSpecificRoles() {
        return Collections.unmodifiableSet(new HashSet<>(Arrays.asList(HubRole.values())));
    }

    @Override
    protected HubPlayer createPlayer(Player player, HubPlayerData playerData) {
        return new HubPlayer(player, playerData);
    }
}