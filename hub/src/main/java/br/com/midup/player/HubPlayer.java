package br.com.midup.player;

import br.com.midup.common.player.BasePlayer;
import br.com.midup.data.HubPlayerData;
import org.bukkit.entity.Player;

public final class HubPlayer extends BasePlayer<HubPlayerData, HubRole> {

    protected HubPlayer(Player player, HubPlayerData playerData) {
        super(player, playerData);
        this.role = HubRole.NORMAL;
    }
}
