package br.com.midup.player;

import br.com.midup.api.player.Role;
import br.com.midup.util.ColorUtil;

import java.util.*;

public enum HubRole implements Role {

    NORMAL(0, "Normal", ColorUtil.GRAY);

    private static final Map<Integer, HubRole> BY_ID = new HashMap<>();
    private static final Map<String, HubRole> BY_ALIAS = new HashMap<>();

    private final int id;
    private final String name;
    private final String color;
    private final Set<String> aliases;

    static {
        for (HubRole role : values()) {
            BY_ID.put(role.id, role);
            BY_ALIAS.put(role.name.toLowerCase(), role);
            BY_ALIAS.put(role.name().toLowerCase(), role);
            for (String alias : role.aliases) {
                BY_ALIAS.put(alias.toLowerCase(), role);
            }
        }
    }

    private HubRole(int id, String name, String color, String... aliases) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.aliases = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(aliases)));
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getColoredName() {
        return color + name;
    }

    @Override
    public Set<String> getAliases() {
        return aliases;
    }

    public static HubRole getRole(int id) {
        return BY_ID.get(id);
    }

    public static HubRole getRole(String alias) {
        return BY_ALIAS.get(alias.toLowerCase());
    }
}
