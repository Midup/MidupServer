package br.com.midup.listener;

import br.com.midup.Midup;
import br.com.midup.common.listener.BaseChatListener;
import br.com.midup.data.HubPlayerData;
import br.com.midup.player.HubPlayer;
import br.com.midup.player.HubRole;

public final class ChatListener extends BaseChatListener<HubPlayer, HubPlayerData, HubRole> {

    public ChatListener(Midup plugin) {
        super(plugin);
    }
}