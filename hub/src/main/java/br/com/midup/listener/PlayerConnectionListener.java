package br.com.midup.listener;

import br.com.midup.Midup;
import br.com.midup.common.listener.BasePlayerConnectionListener;
import br.com.midup.data.HubPlayerData;
import br.com.midup.player.HubPlayer;
import br.com.midup.player.HubRole;

public final class PlayerConnectionListener extends BasePlayerConnectionListener<HubPlayer, HubPlayerData, HubRole> {

    public PlayerConnectionListener(Midup plugin) {
        super(plugin);
    }
}
