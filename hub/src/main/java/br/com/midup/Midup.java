package br.com.midup;

import br.com.midup.common.MidupCommon;
import br.com.midup.common.networking.ServerType;
import br.com.midup.data.HubDataHandler;
import br.com.midup.data.HubPlayerData;
import br.com.midup.listener.ChatListener;
import br.com.midup.listener.PlayerConnectionListener;
import br.com.midup.player.HubPlayer;
import br.com.midup.player.HubPlayerList;
import br.com.midup.player.HubRole;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

public final class Midup extends MidupCommon<HubPlayer, HubPlayerData, HubRole> {

    private static Midup instance;

    private final HubDataHandler dataHandler;
    private final HubPlayerList playerList;

    public Midup() {
        super();

        instance = this;
        this.dataHandler = new HubDataHandler(this);
        this.playerList = new HubPlayerList();
    }

    @Override
    public void onEnable() {
        super.onEnable();

        logger.log("Starting database module...");
        this.dataHandler.loadConfiguration();
        this.dataHandler.openConnection();

        PluginManager manager = Bukkit.getPluginManager();
        manager.registerEvents(new PlayerConnectionListener(this), this);
        manager.registerEvents(new ChatListener(this), this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public String getVersion() {
        return getClass().getPackage().getImplementationVersion();
    }

    @Override
    public ServerType getServerType() {
        return ServerType.HUB;
    }

    @Override
    public HubDataHandler getDataHandler() {
        return dataHandler;
    }

    @Override
    public HubPlayerList getPlayerList() {
        return playerList;
    }

    public static Midup getInstance() {
        return instance;
    }
}
