package br.com.midup.data.player;

import br.com.midup.game.BedGameMode;

public class BedPlayerData {

    private int victories;
    private int matchs;
    private int finalKills;
    private int generalKills;
    private int supports;
    private int bedsDestroyed;
    private int teamsEliminated;

    private BedGameMode mode;

    public BedPlayerData(BedGameMode mode, int victories, int matchs, int finalKills, int generalKills, int supports, int bedsDestroyed, int teamsEliminated) {
        this.mode = mode;
        this.victories = victories;
        this.matchs = matchs;
        this.finalKills = finalKills;
        this.generalKills = generalKills;
        this.supports = supports;
        this.bedsDestroyed = bedsDestroyed;
        this.teamsEliminated = teamsEliminated;
    }

    public int getVictories() {
        return victories;
    }

    public void setVictories(int victories) {
        this.victories = victories;
    }

    public int getMatchs() {
        return matchs;
    }

    public void setMatchs(int matchs) {
        this.matchs = matchs;
    }

    public int getFinalKills() {
        return finalKills;
    }

    public void setFinalKills(int finalKills) {
        this.finalKills = finalKills;
    }

    public int getGeneralKills() {
        return generalKills;
    }

    public void setGeneralKills(int generalKills) {
        this.generalKills = generalKills;
    }

    public int getSupports() {
        return supports;
    }

    public void setSupports(int supports) {
        this.supports = supports;
    }

    public int getBedsDestroyed() {
        return bedsDestroyed;
    }

    public void setBedsDestroyed(int bedsDestroyed) {
        this.bedsDestroyed = bedsDestroyed;
    }

    public int getTeamsEliminated() {
        return teamsEliminated;
    }

    public void setTeamsEliminated(int teamsEliminated) {
        this.teamsEliminated = teamsEliminated;
    }

    public BedGameMode getGameMode() {
        return mode;
    }

    public void setGameMode(BedGameMode mode) {
        this.mode = mode;
    }

    @Override
    public String toString() {
        return String.format("{ mode: %s, victories: %d, matchs: %d, final_kills: %d, general_kills: %d, " +
                        "supports: %d, beds_destroyed: %d, teams_eliminated: %d }", mode, victories, matchs,
                finalKills, generalKills, supports, bedsDestroyed, teamsEliminated);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof BedPlayerData)) {
            return false;
        }

        BedPlayerData playerMode = (BedPlayerData) o;
        return this.mode == playerMode.mode;
    }

    @Override
    public int hashCode() {
        return this.mode.getId();
    }
}
