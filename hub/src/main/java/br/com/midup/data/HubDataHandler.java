package br.com.midup.data;

import br.com.midup.Midup;
import br.com.midup.common.data.BaseDataHandler;
import br.com.midup.common.networking.ServerType;
import br.com.midup.data.player.BedPlayerData;
import br.com.midup.game.BedGameMode;
import br.com.midup.player.HubPlayer;
import br.com.midup.player.HubRole;
import org.bson.Document;

import java.util.*;

public class HubDataHandler extends BaseDataHandler<HubPlayer, HubPlayerData, HubRole> {

    public HubDataHandler(Midup plugin) {
        super(plugin);
    }

    @Override
    public void loadSpecificPlayerData(HubPlayerData playerData, Document document) {
        int coins = document.getInteger("coins");
        playerData.setCoins(coins);
    }

    @Override
    public void updateSpecificPlayerData(HubPlayerData playerData) {
        Document filter = new Document("_id", playerData.getUniqueId());
        Document document = new Document("coins", playerData.getCoins());

        this.specificCollection.updateOne(filter, new Document("$set", document), UPSERT);
    }

    @Override
    public HubPlayerData initializePlayerData(UUID uniqueId, String name) {
        return new HubPlayerData(uniqueId, name);
    }
}
