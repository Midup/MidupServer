package br.com.midup.data;

import br.com.midup.common.data.BasePlayerData;
import br.com.midup.data.player.BedPlayerData;
import br.com.midup.game.BedGameMode;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class HubPlayerData extends BasePlayerData {

    private int coins;

    private Map<BedGameMode, BedPlayerData> bedPlayerData;

    protected HubPlayerData(UUID uniqueId, String name) {
        super(uniqueId, name);
        this.coins = 0;
        this.bedPlayerData = new HashMap<>();
    }

    public Map<BedGameMode, BedPlayerData> getBedPlayerData() {
        return bedPlayerData;
    }

    public void addBedPlayerData(BedPlayerData gameData) {
        if (bedPlayerData.containsKey(gameData)) {
            return;
        }
        bedPlayerData.put(gameData.getGameMode(), gameData);
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int amount) {
        this.coins = amount;
    }
}