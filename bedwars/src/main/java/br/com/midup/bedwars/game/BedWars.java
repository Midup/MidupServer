package br.com.midup.bedwars.game;

import br.com.midup.bedwars.MidupBedWars;
import br.com.midup.bedwars.data.BedGameData;
import br.com.midup.common.logging.FormattedLogger;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;

public class BedWars {

    private static final String CONFIG_FILE_NAME = "game.yml";

    private MidupBedWars plugin;

    private File configFile;
    private FileConfiguration config;
    private FormattedLogger logger;

    private BedGameData gameData;
    private BukkitTask handleTask;

    private BedGameMode mode;

    private boolean isRunning;

    public BedWars(MidupBedWars plugin) {
        this.plugin = plugin;
        this.configFile = new File(plugin.getDataFolder(), CONFIG_FILE_NAME);
        this.logger = new FormattedLogger(plugin.getParentLogger(), "Game");
        this.gameData = null;
        this.handleTask = null;
        this.mode = null;
        this.isRunning = false;
    }

    public void loadConfiguration() {
        if (!configFile.exists()) {
            plugin.saveResource(CONFIG_FILE_NAME, true);
        }

        this.config = YamlConfiguration.loadConfiguration(configFile);
        this.gameData = BedGameData.create(BedGameMode.getGameType(config.getString("GameType")));
        this.mode = gameData.getGameMode();
    }

    public void startGame() {
        if (isRunning) {
            return;
        }

        logger.log("[Game] Starting game...");
        loadConfiguration();

        this.isRunning = true;
    }

    public void stopGame(String reason) {
        if (!isRunning) {
            return;
        }

        this.isRunning = false;

        if (handleTask != null) {
            handleTask.cancel();
        }

        Bukkit.broadcastMessage(reason);
    }

    public void setRunning(boolean run) {
        this.isRunning = run;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void cancelGameTask() {
        handleTask.cancel();
    }

    public BedGameData getGameData() {
        return gameData;
    }

    public BedGameMode getGameMode() {
        return mode;
    }
}
