package br.com.midup.bedwars.game.kit;

import br.com.midup.bedwars.game.kit.executor.EmptyKitExecutor;
import br.com.midup.util.ColorUtil;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public enum Kit {

    NONE(-1, "Nenhum", "Nenhum kit selecionado", new EmptyKitExecutor());

    private static final Map<Integer, Kit> BY_ID = new HashMap<>();
    private static final Map<String, Kit> BY_NAME = new HashMap<>();

    private int id;
    private String name;
    private String description;
    private KitExecutor executor;
    private boolean free;

    static {
        for (Kit kit : values()) {
            BY_ID.put(kit.id, kit);
            if (kit == NONE) {
                continue;
            }
            BY_NAME.put(kit.name.toLowerCase(), kit);
        }
    }

    private Kit(int id, String name, String description, KitExecutor executor) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.executor = executor;
        this.free = id < 0;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public KitExecutor getExecutor() {
        return executor;
    }

    public boolean isFree() {
        return this.free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public static Kit getKit(int id) {
        return BY_ID.get(id);
    }

    public static Kit getKit(String alias) {
        return BY_NAME.get(alias.toLowerCase());
    }

    public static String getNameList(Collection<Kit> kits, boolean addColor) {
        StringBuilder builder = new StringBuilder();
        for (Kit kit : kits) {
            builder.append(ColorUtil.WHITE).append(", ").append(addColor ? ColorUtil.AQUA : "").append(kit.name);
        }
        return builder.length() < 1 ? "" : builder.substring(4);
    }
}