package br.com.midup.bedwars.player;

import br.com.midup.bedwars.data.BedPlayerData;
import br.com.midup.bedwars.game.BedGameMode;
import br.com.midup.common.player.BasePlayer;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BedPlayer extends BasePlayer<BedPlayerData, BedRole> {

    private Map<BedGameMode, BedPlayerData> playerData;
    private Set<PlayerKit> kits;

    protected BedPlayer(Player player, BedPlayerData playerData) {
        super(player, playerData);
        this.role = BedRole.NORMAL;
        this.playerData = new HashMap<>();
        this.kits = new HashSet<>();
    }

    public void clearInventory(boolean inventory, boolean armor, boolean effects) {
        if (inventory) {
            player.getInventory().clear();
        }
        if (armor) {
            player.getInventory().setArmorContents(null);
        }
        if (effects) {
            for (PotionEffect effect : player.getActivePotionEffects()) {
                player.removePotionEffect(effect.getType());
            }
        }
    }
}