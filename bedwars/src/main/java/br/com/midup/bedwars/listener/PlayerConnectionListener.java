package br.com.midup.bedwars.listener;

import br.com.midup.bedwars.MidupBedWars;
import br.com.midup.bedwars.data.BedPlayerData;
import br.com.midup.bedwars.player.BedPlayer;
import br.com.midup.bedwars.player.BedRole;
import br.com.midup.common.listener.BasePlayerConnectionListener;
import br.com.midup.util.ColorUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

public final class PlayerConnectionListener
        extends BasePlayerConnectionListener<BedPlayer, BedPlayerData, BedRole> {

    private final MidupBedWars plugin;

    public PlayerConnectionListener(MidupBedWars plugin) {
        super(plugin);
        this.plugin = plugin;
    }

    @EventHandler
    public final void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        BedPlayer bPlayer = plugin.getPlayerList().getPlayer(player);

        bPlayer.getData().setVictories(bPlayer.getData().getVictories() + 1);

        bPlayer.getPlayer().sendMessage(ColorUtil.YELLOW + "Your victories " + ColorUtil.AQUA + bPlayer.getData().getVictories() + " in mode " + plugin.getBedGame().getGameMode().getColoredName());
    }
}
