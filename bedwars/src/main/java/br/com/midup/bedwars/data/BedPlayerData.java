package br.com.midup.bedwars.data;

import br.com.midup.bedwars.player.PlayerKit;
import br.com.midup.common.data.BasePlayerData;

import java.util.*;

public final class BedPlayerData extends BasePlayerData {

    private int victories;
    private int matchs;
    private int finalKills;
    private int generalKills;
    private int supports;
    private int bedsDestroyed;
    private int teamsEliminated;
    private int coins;

    private Set<PlayerKit> kits;

    protected BedPlayerData(UUID uniqueId, String name) {
        super(uniqueId, name);
        this.victories = 0;
        this.matchs = 0;
        this.finalKills = 0;
        this.generalKills = 0;
        this.supports = 0;
        this.bedsDestroyed = 0;
        this.teamsEliminated = 0;
        this.coins = 0;
        this.kits = new HashSet<>();
    }

    public int getVictories() {
        return victories;
    }

    public void setVictories(int victories) {
        this.victories = victories;
    }

    public int getMatchs() {
        return matchs;
    }

    public void setMatchs(int matchs) {
        this.matchs = matchs;
    }

    public int getFinalKills() {
        return finalKills;
    }

    public void setFinalKills(int finalKills) {
        this.finalKills = finalKills;
    }

    public int getGeneralKills() {
        return generalKills;
    }

    public void setGeneralKills(int generalKills) {
        this.generalKills = generalKills;
    }

    public int getSupports() {
        return supports;
    }

    public void setSupports(int supports) {
        this.supports = supports;
    }

    public int getBedsDestroyed() {
        return bedsDestroyed;
    }

    public void setBedsDestroyed(int bedsDestroyed) {
        this.bedsDestroyed = bedsDestroyed;
    }

    public int getTeamsEliminated() {
        return teamsEliminated;
    }

    public void setTeamsEliminated(int teamsEliminated) {
        this.teamsEliminated = teamsEliminated;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public Set<PlayerKit> getKits() {
        return kits;
    }
}