package br.com.midup.bedwars.player;

import br.com.midup.bedwars.game.kit.Kit;
import br.com.midup.util.Util;

public class PlayerKit {

    private Kit kit;
    private int expiry;

    public PlayerKit(Kit kit) {
        this(kit, -1);
    }

    public PlayerKit(Kit kit, int expiry) {
        this.kit = kit;
        this.expiry = expiry;
    }

    public Kit getKit() {
        return kit;
    }

    public void setKit(Kit kit) {
        this.kit = kit;
    }

    public int getExpiry() {
        return expiry;
    }

    public void setExpiry(int expiry) {
        this.expiry = expiry;
    }

    public boolean isExpired() {
        return expiry != -1 && expiry < Util.unixTimestamp();
    }

    @Override
    public String toString() {
        return String.format("{ mode: %s, expiry: %d }", kit, expiry);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof PlayerKit)) {
            return false;
        }

        PlayerKit playerKit = (PlayerKit) o;
        return this.kit == playerKit.kit;
    }

    @Override
    public int hashCode() {
        return this.kit.getId();
    }
}