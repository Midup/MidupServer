package br.com.midup.bedwars.game.kit;

import br.com.midup.bedwars.MidupBedWars;
import br.com.midup.bedwars.player.BedPlayer;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.List;

public abstract class KitExecutor {

    protected MidupBedWars plugin;

    protected ItemStack[] inventoryContents;
    protected ItemStack[] armorContents;
    protected List<PotionEffect> potionEffects;

    public KitExecutor() {
        this.plugin = MidupBedWars.getInstance();
        this.inventoryContents = new ItemStack[36];
        this.armorContents = new ItemStack[4];
        this.potionEffects = new ArrayList<>();
    }

    public final void giveKitItems(BedPlayer bPlayer) {
        Player player = bPlayer.getPlayer();
        bPlayer.clearInventory(true, true, true);
        PlayerInventory inventory = player.getInventory();
        inventory.setContents(inventoryContents);
        inventory.setArmorContents(armorContents);
        player.addPotionEffects(potionEffects);
    }

    public final boolean hasCooldown(BedPlayer bPlayer) {
        return plugin.getCooldownManager().hasCooldown("kitCooldown", bPlayer);
    }

    public final void applyCooldown(BedPlayer bPlayer, long millis) {
        if(hasCooldown(bPlayer)) plugin.getCooldownManager().removeCooldown("kitCooldown", bPlayer);
        plugin.getCooldownManager().addCooldown("kitCooldown", bPlayer, millis);
    }

    public void onSelectKit(BedPlayer bPlayer) {}

    public void onDeselectKit(BedPlayer bPlayer) {}

    public void onTakeDamage(BedPlayer bPlayer, EntityDamageEvent event) {}

    public void onTakeDamageByPlayer(BedPlayer bPlayer, BedPlayer bDamager, EntityDamageByEntityEvent event) {}

    public void onTakeDamageByPlayerProjectile(BedPlayer bPlayer, BedPlayer bDamager, Projectile projectile, EntityDamageByEntityEvent event) {}

    public void onTakeDamageByProjectile(BedPlayer bPlayer, Projectile projectile, EntityDamageByEntityEvent event) {}

    public void onTakeDamageByEntity(BedPlayer bPlayer, EntityDamageByEntityEvent event) {}

    public void onTakeDamageByBlock(BedPlayer bPlayer, EntityDamageByBlockEvent event) {}

    public void onDealDamageToPlayer(BedPlayer bPlayer, BedPlayer bDamaged, EntityDamageByEntityEvent event) {}

    public void onDealDamageToEntity(BedPlayer bPlayer, EntityDamageByEntityEvent event) {}

    public void onDealDamageToPlayerViaProjectile(BedPlayer bPlayer, BedPlayer bDamaged, Projectile projectile, EntityDamageByEntityEvent event) {}

    public void onDealDamageToEntityViaProjectile(BedPlayer bPlayer, Projectile projectile, EntityDamageByEntityEvent event) {}

    public void onInteract(BedPlayer bPlayer, PlayerInteractEvent event) {}

    public void onInteractWithPlayer(BedPlayer bPlayer, BedPlayer bClicked, PlayerInteractEntityEvent event) {}

    public void onInteractWithEntity(BedPlayer bPlayer, PlayerInteractEntityEvent event) {}

    public void onThrowFishingLine(BedPlayer bPlayer, PlayerFishEvent event) {}

    public void onFishPlayer(BedPlayer bPlayer, BedPlayer bFished, PlayerFishEvent event) {}

    public void onFishEntity(BedPlayer bPlayer, PlayerFishEvent event) {}

    public void onMove(BedPlayer bPlayer, PlayerMoveEvent event) {}

    public void onDeathByPlayer(BedPlayer bPlayer, BedPlayer bKiller, PlayerDeathEvent event) {}

    public void onDeath(BedPlayer bPlayer, PlayerDeathEvent event) {}

    public void onKillPlayer(BedPlayer bPlayer, BedPlayer bKilled, PlayerDeathEvent event) {}

    public void onKillEntity(BedPlayer bPlayer, EntityDeathEvent event) {}

    public void onSneak(BedPlayer bPlayer, PlayerToggleSneakEvent event) {}

    public void onBlockPlace(BedPlayer bPlayer, BlockPlaceEvent event) {}

    public void onBlockBreak(BedPlayer bPlayer, BlockBreakEvent event) {}
}