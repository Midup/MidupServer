package br.com.midup.bedwars.game;

import br.com.midup.util.ColorUtil;

import java.util.HashMap;
import java.util.Map;

public enum BedGameMode {

    SOLO(0, "Solo", ColorUtil.YELLOW),
    DUO(1, "Dupla", ColorUtil.GOLD),
    TRIO(2, "Trio", ColorUtil.RED),
    QUARTET(3, "Quarteto", ColorUtil.BLUE);

    private static final Map<Integer, BedGameMode> BY_ID = new HashMap<>();
    private static final Map<String, BedGameMode> BY_NAME = new HashMap<>();

    private final int id;
    private final String name;
    private final String color;

    static {
        for (BedGameMode role : values()) {
            BY_ID.put(role.id, role);
            BY_NAME.put(role.name(), role);
            BY_NAME.put(role.name, role);
        }
    }

    private BedGameMode(int id, String name, String color) {
        this.id = id;
        this.name = name;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public String getColoredName() {
        return color + name;
    }

    public static BedGameMode getGameType(int id) {
        return BY_ID.get(id);
    }

    public static BedGameMode getGameType(String name) {
        return BY_NAME.get(name);
    }
}
