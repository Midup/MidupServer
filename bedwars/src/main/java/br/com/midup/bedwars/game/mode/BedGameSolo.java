package br.com.midup.bedwars.game.mode;

import br.com.midup.bedwars.data.BedGameData;
import br.com.midup.bedwars.game.BedGameMode;

public class BedGameSolo extends BedGameData {

    private int maxPlayers;
    private int minPlayers;

    public BedGameSolo() {
        super(BedGameMode.SOLO);

        this.maxPlayers = 16;
        this.minPlayers = 14;
    }

    @Override
    public int getMaxPlayers() {
        return maxPlayers;
    }

    @Override
    public int getMinPlayers() {
        return minPlayers;
    }
}

