package br.com.midup.bedwars.listener;

import br.com.midup.bedwars.MidupBedWars;
import br.com.midup.bedwars.data.BedPlayerData;
import br.com.midup.bedwars.player.BedPlayer;
import br.com.midup.bedwars.player.BedRole;
import br.com.midup.common.listener.BaseChatListener;

public final class ChatListener extends BaseChatListener<BedPlayer, BedPlayerData, BedRole> {

    public ChatListener(MidupBedWars plugin) {
        super(plugin);
    }
}
