package br.com.midup.bedwars.player;

import br.com.midup.bedwars.data.BedPlayerData;
import br.com.midup.common.player.BasePlayerList;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class BedPlayerList extends BasePlayerList<BedPlayer, BedPlayerData, BedRole> {

    @Override
    public BedRole getSpecificRole(int id) {
        return BedRole.getRole(id);
    }

    @Override
    public Set<BedRole> getSpecificRoles() {
        return Collections.unmodifiableSet(new HashSet<>(Arrays.asList(BedRole.values())));
    }

    @Override
    protected BedPlayer createPlayer(Player player, BedPlayerData playerData) {
        return new BedPlayer(player, playerData);
    }
}