package br.com.midup.bedwars.data;

import br.com.midup.bedwars.game.BedGameMode;
import br.com.midup.bedwars.game.mode.*;

public abstract class BedGameData {

    protected final BedGameMode mode;

    public BedGameData(BedGameMode mode) {
        this.mode = mode;
    }

    public static BedGameData create(BedGameMode mode) {
        switch (mode) {
            case SOLO:
                return new BedGameSolo();
            case DUO:
                return new BedGameDuo();
            case TRIO:
                return new BedGameTrio();
            case QUARTET:
                return new BedGameQuartet();

            default:
                throw new IllegalArgumentException("Illegal gameType.");
        }
    }

    public enum Generators {

        DEFAULT(),
        DIAMOND(),
        EMERALD();
    }

    public BedGameMode getGameMode() {
        return this.mode;
    }

    public abstract int getMaxPlayers();

    public abstract int getMinPlayers();

}
