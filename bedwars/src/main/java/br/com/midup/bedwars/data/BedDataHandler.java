package br.com.midup.bedwars.data;

import br.com.midup.bedwars.MidupBedWars;
import br.com.midup.bedwars.game.kit.Kit;
import br.com.midup.bedwars.game.BedGameMode;
import br.com.midup.bedwars.player.BedPlayer;
import br.com.midup.bedwars.player.BedRole;
import br.com.midup.bedwars.player.PlayerKit;
import br.com.midup.common.data.BaseDataHandler;
import org.bson.Document;

import java.util.*;

public class BedDataHandler extends BaseDataHandler<BedPlayer, BedPlayerData, BedRole> {

    private MidupBedWars plugin;

    public BedDataHandler(MidupBedWars plugin) {
        super(plugin);
        this.plugin = plugin;
    }

    @Override
    public void loadSpecificPlayerData(BedPlayerData playerData, Document document) {
        playerData.setCoins(document.getInteger("coins"));

        List<?> kitsDocument = document.get("kits", List.class);
        for (Object kitDocumentObj : kitsDocument) {
            Document kitDocument = (Document) kitDocumentObj;

            int id = kitDocument.getInteger("id");
            int expiry = kitDocument.getInteger("expiry");
            Kit kit = Kit.getKit(id);

            if (kit == null) {
                throw new IllegalArgumentException("Invalid kit id: " + id);
            }

            playerData.getKits().add(new PlayerKit(kit, expiry));
        }

        List<?> bedDataDocumentList = document.get("modes", List.class);
        for (Object bedDataDocumentObj : bedDataDocumentList) {
            Document bedDataDocument = (Document) bedDataDocumentObj;

            int id = bedDataDocument.getInteger("id");
            BedGameMode mode = BedGameMode.getGameType(id);
            if (mode == null) {
                throw new IllegalArgumentException("Invalid gameMode id: " + id);
            }

            if (mode.equals(plugin.getBedGame().getGameMode())) {
                playerData.setVictories(bedDataDocument.getInteger("victories"));
                playerData.setMatchs(bedDataDocument.getInteger("matchs"));
                playerData.setFinalKills(bedDataDocument.getInteger("final_kills"));
                playerData.setGeneralKills(bedDataDocument.getInteger("general_kills"));
                playerData.setSupports(bedDataDocument.getInteger("supports"));
                playerData.setBedsDestroyed(bedDataDocument.getInteger("beds_destroyed"));
                playerData.setTeamsEliminated(bedDataDocument.getInteger("teams_eliminated"));
            }

        }
    }

    @Override
    protected void updateSpecificPlayerData(BedPlayerData playerData) {
        Document filter = new Document("_id", playerData.getUniqueId());
        Document document = new Document("coins", playerData.getCoins());
        List<Document> playerKits = new ArrayList<>();
        for (PlayerKit playerKit : playerData.getKits()) {
            Document playerKitDoc = new Document("id", playerKit.getKit().getId());
            playerKitDoc.append("expiry", playerKit.getExpiry());
            playerKits.add(playerKitDoc);
        }

        Document playerDataDoc = new Document("id", plugin.getBedGame().getGameMode().getId());
        playerDataDoc.append("victories", playerData.getVictories());
        playerDataDoc.append("matchs", playerData.getMatchs());
        playerDataDoc.append("final_kills", playerData.getFinalKills());
        playerDataDoc.append("general_kills", playerData.getGeneralKills());
        playerDataDoc.append("supports", playerData.getSupports());
        playerDataDoc.append("beds_destroyed", playerData.getBedsDestroyed());
        playerDataDoc.append("teams_eliminated", playerData.getTeamsEliminated());

        document.append("kits", playerKits);
        document.append("modes", playerDataDoc);
        this.specificCollection.updateOne(filter, new Document("$set", document), UPSERT);
    }

    @Override
    public BedPlayerData initializePlayerData(UUID uniqueId, String name) {
        return new BedPlayerData(uniqueId, name);
    }
}