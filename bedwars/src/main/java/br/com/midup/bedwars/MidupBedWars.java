package br.com.midup.bedwars;

import br.com.midup.bedwars.data.BedDataHandler;
import br.com.midup.bedwars.data.BedPlayerData;
import br.com.midup.bedwars.game.BedWars;
import br.com.midup.bedwars.listener.ChatListener;
import br.com.midup.bedwars.listener.PlayerConnectionListener;
import br.com.midup.bedwars.player.BedPlayer;
import br.com.midup.bedwars.player.BedPlayerList;
import br.com.midup.bedwars.player.BedRole;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.networking.ServerType;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

public class MidupBedWars extends MidupCommon<BedPlayer, BedPlayerData, BedRole> {

    private static MidupBedWars instance;

    private final BedDataHandler dataHandler;
    private final BedPlayerList playerList;

    private BedWars bedGame;

    public MidupBedWars() {
        super();
        instance = this;
        this.dataHandler = new BedDataHandler(this);
        this.playerList = new BedPlayerList();
        this.bedGame = new BedWars(this);
    }

    @Override
    public void onEnable() {
        super.onEnable();

        logger.log("Starting database module...");
        this.dataHandler.loadConfiguration();
        this.dataHandler.openConnection();

        PluginManager manager = Bukkit.getPluginManager();
        manager.registerEvents(new PlayerConnectionListener(this), this);
        manager.registerEvents(new ChatListener(this), this);

        bedGame.startGame();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public ServerType getServerType() {
        return ServerType.BEDWARS;
    }

    @Override
    public BedDataHandler getDataHandler() {
        return dataHandler;
    }

    @Override
    public BedPlayerList getPlayerList() {
        return playerList;
    }

    public static MidupBedWars getInstance() {
        return instance;
    }

    public BedWars getBedGame() {
        return bedGame;
    }
}
