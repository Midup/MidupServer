package br.com.midup.util;

import br.com.midup.api.player.Group;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public final class Util {

    public static final String CONSOLE_NAME = Group.values()[0].getColor() + "@Console";
    public static final UUID CONSOLE_UUID = UUID.fromString("00000000-0000-0000-0000-000000000000");

    private static final String MONGO_LOGGER_PREFIX = "org.mongodb.driver";
    private static final List<String> MONGO_LOGGERS = Arrays.asList("client.jndi", "cluster", "cluster.event",
            "connection", "management", "operation", "protocol.command", "protocol.delete", "protocol.event",
            "protocol.getmore", "protocol.insert", "protocol.killcursor", "protocol.query", "protocol.update", "uri");

    private static final Pattern TIME_STRING_TOKENIZER = Pattern.compile("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");

    private Util() {
    }

    public static void disableMongoLoggers() {
        for (String logger : MONGO_LOGGERS) {
            Logger.getLogger(MONGO_LOGGER_PREFIX + "." + logger).setLevel(Level.OFF);
        }
    }

    public static int unixTimestamp() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    public static long toJavaTimestamp(int unixTimestamp) {
        return unixTimestamp * 1000;
    }

    public static int toUnixTimestamp(long javaTimestamp) {
        return (int) (javaTimestamp / 1000);
    }

    public static UUID uniqueId(String uniqueIdText) {
        try {
            return UUID.fromString(uniqueIdText);
        } catch (Exception e) {
            return null;
        }
    }

    public static String stringJoin(String[] args, String joint) {
        return stringJoin(Arrays.asList(args), joint);
    }

    public static String stringJoin(Collection<String> args, String joint) {
        StringBuilder builder = new StringBuilder();
        for (String arg : args) {
            builder.append(joint).append(arg);
        }
        return builder.length() < 1 ? "" : builder.substring(joint.length());
    }

    public static String coloredStringJoin(ColoredName[] args, String joint) {
        return coloredStringJoin(Arrays.asList(args), joint);
    }

    public static String coloredStringJoin(Collection<? extends ColoredName> args, String joint) {
        StringBuilder builder = new StringBuilder();
        for (ColoredName arg : args) {
            builder.append(joint).append(arg.getColoredName());
        }
        return builder.length() < 1 ? "" : builder.substring(joint.length());
    }

    public static String getDisplayName(CommandSender sender) {
        return sender instanceof Player ? ((Player) sender).getDisplayName() : CONSOLE_NAME;
    }

    public static int parseTimeString(String timeString) {
        String[] tokens = TIME_STRING_TOKENIZER.split(timeString);
        int totalSeconds = 0;

        if (tokens.length % 2 != 0) {
            System.out.println("Invalid length");
            return -1;
        }

        try {
            for (int i = 0; i < tokens.length; i += 2) {
                int number = Integer.parseInt(tokens[i]);
                if (tokens[i + 1].length() > 1) {
                    System.out.println("Not a character");
                    return -1;
                }
                char type = Character.toLowerCase(tokens[i + 1].charAt(0));

                switch (type) {
                    case 's':
                        totalSeconds += number;
                        break;

                    case 'm':
                        totalSeconds += number * 60;
                        break;

                    case 'h':
                        totalSeconds += number * 3600;
                        break;

                    case 'd':
                        totalSeconds += number * 86400;
                        break;

                    default:
                        System.out.println("Invalid mode");
                        return -1;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return totalSeconds;
    }

    public static String getLongTimeString(int totalSeconds) {
        return getLongTimeString(totalSeconds, null, null);
    }

    public static String getLongTimeString(int totalSeconds, String numberPrefix, String textPrefix) {
        StringBuilder builder = new StringBuilder();

        for (TimeUnit unit : TimeUnit.values()) {
            if (totalSeconds >= unit.totalSeconds) {
                int amount = totalSeconds / unit.totalSeconds;
                if (amount > 0) {
                    if (numberPrefix != null) {
                        builder.append(numberPrefix);
                    }
                    builder.append(amount).append(' ');
                    if (textPrefix != null) {
                        builder.append(textPrefix);
                    }
                    builder.append(amount == 1 ? unit.singular : unit.plural).append(' ');
                    totalSeconds %= unit.totalSeconds;
                }
            }
        }
        return builder.toString().trim();
    }

    public static String getTimeString(int seconds) {
        int h = seconds / 3600;
        int m = (seconds % 3600) / 60;
        int s = seconds % 60;

        return h > 0 ? String.format("%01d:%02d:%02d", h, m, s) : String.format("%01d:%02d", m, s);
    }

    public static <K, V> void removeAll(Map<K, V> map, Set<? extends K> values) {
        for (K value : values) {
            map.remove(value);
        }
    }

    public static <T> boolean contains(T target, T... elements) {
        for (T element : elements) {
            if (target.equals(element)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isInteger(String str) {
        try {
            Integer.valueOf(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static enum TimeUnit {
        DAY(86400, "dias", "dia"),
        HOUR(3600, "horas", "hora"),
        MINUTE(60, "minutos", "minuto"),
        SECOND(1, "segundos", "segundo");

        private final int totalSeconds;
        private final String plural;
        private final String singular;

        private TimeUnit(int totalSeconds, String plural, String singular) {
            this.totalSeconds = totalSeconds;
            this.plural = plural;
            this.singular = singular;
        }
    }
}