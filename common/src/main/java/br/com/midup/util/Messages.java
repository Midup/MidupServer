package br.com.midup.util;

public final class Messages {

    public static final String PLAYER_NOT_FOUND = ColorUtil.RED + "The player '%s' is not online!";

    public static final String PLAYER_NOT_FOUND_SEARCHING_DB = ColorUtil.RED
            + "The player '%s' is not online! Searching on the database...";

    public static final String UUID_NOT_FOUND = ColorUtil.RED + "The player by the unique id of '%s' is not online!";

    public static final String UUID_NOT_FOUND_SEARCHING_DB = ColorUtil.RED
            + "The player by the unique id of '%s' is not online! Searching on the database...";

    public static final String MULTIPLE_FOUND = ColorUtil.RED
            + "Multiple players by the name of '%s' have been found! Use the player's Unique ID instead!";

    public static final String PLAYER_NOT_IN_DB = ColorUtil.RED + "The player '%s' is not registered on the database!";

    public static final String UUID_NOT_IN_DB = ColorUtil.RED
            + "The player by the unique id of '%s' is not registered on the database!";

    public static final String PLAYER_DATA_FIND_ERROR = ColorUtil.RED
            + "An error occurred while trying to find the data from the specified player from the database!";

    public static final String PLAYER_DATA_UPDATE_ERROR = ColorUtil.RED
            + "An error occurred while trying to update the data from the specified player to the database!";

    public static final String CANNOT_SELF_BAN = ColorUtil.RED + "You cannot ban yourself!";

    public static final String CANNOT_SELF_MUTE = ColorUtil.RED + "You cannot mute yourself!";

    public static final String CANNOT_BAN_STAFF = ColorUtil.RED + "You cannot ban a staff member!";

    public static final String CANNOT_MUTE_STAFF = ColorUtil.RED + "You cannot mute a staff member!";

    public static final String ALREADY_BANNED = ColorUtil.RED + "The player '%s' is already banned!";

    public static final String ALREADY_MUTED = ColorUtil.RED + "The player '%s' is already muted!";

    public static final String INVALID_TIME_STRING = ColorUtil.RED + "The time string '%s' is not valid!";

    public static final String YOUR_GROUP_SET = ColorUtil.GRAY + "You're now a %s" + ColorUtil.GRAY + "!";

    public static final String YOU_HAVE_BEEN_BANNED = ColorUtil.BOLD_C + "You've been banned!" + ColorUtil.RED
            + "\nBanned by: %s" + ColorUtil.RED + "\nReason: " + ColorUtil.RESET + "%s";

    public static final String YOU_HAVE_BEEN_TEMP_BANNED = ColorUtil.BOLD_C + "You've been temporarily banned!"
            + ColorUtil.RED + "\nBanned by: %s" + ColorUtil.RED + "\nReason: " + ColorUtil.RESET + "%s" + ColorUtil.RED
            + "\nExpires in: " + ColorUtil.RESET + "%s";

    public static final String NONE = ColorUtil.RED + "---";

    public static final String JOINT = ColorUtil.RESET + ", ";

    public static final String UNKNOWN = ColorUtil.RESET + "???";

    private Messages() {}
}