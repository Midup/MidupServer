package br.com.midup.util;

public interface ColoredName {

    public String getColoredName();
}