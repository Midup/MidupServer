package br.com.midup.api.data;

import br.com.midup.util.Util;
import org.bson.Document;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Set;

import static br.com.midup.api.data.DataField.DataFieldOption.BANS;
import static br.com.midup.api.data.DataField.DataFieldOption.GROUP;
import static br.com.midup.api.data.DataField.DataFieldOption.MUTES;

public final class DataField {

    private final Set<DataFieldOption> selectedOptions;

    private DataField(DataFieldOption... options) {
        this.selectedOptions = EnumSet.noneOf(DataFieldOption.class);
        selectedOptions.addAll(Arrays.asList(options));
    }

    public boolean has(DataFieldOption option) {
        return selectedOptions.contains(option);
    }

    public DataField with(DataFieldOption option) {
        this.selectedOptions.add(option);
        return this;
    }

    public DataField without(DataFieldOption option) {
        this.selectedOptions.remove(option);
        return this;
    }

    public Document project(String... extraFields) {
        Document projects = new Document();
        for (DataFieldOption option : selectedOptions) {
            projects.append(option.getFieldName(), true);
        }
        for (String extraField : extraFields) {
            projects.append(extraField, true);
        }
        return new Document("$project", projects);
    }

    public static DataField of(DataFieldOption... options) {
        return new DataField(options);
    }

    public static DataField all() {
        return new DataField(DataFieldOption.values());
    }

    public static DataField exceptCritical() {
        return except(GROUP, BANS, MUTES);
    }

    public static DataField except(DataFieldOption... options) {
        DataField dataField = new DataField();
        for (DataFieldOption option : DataFieldOption.values()) {
            if (!Util.contains(option, options)) {
                dataField.selectedOptions.add(option);
            }
        }
        return dataField;
    }

    public static enum DataFieldOption {
        NAME("name"),
        GROUP("group"),
        GROUP_EXPIRY("group-expiry"),
        BANS("bans"),
        MUTES("mutes");

        private final String fieldName;

        private DataFieldOption(String fieldName) {
            this.fieldName = fieldName;
        }

        public String getFieldName() {
            return fieldName;
        }
    }
}