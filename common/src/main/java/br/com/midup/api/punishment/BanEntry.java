package br.com.midup.api.punishment;

import br.com.midup.util.Util;

import java.util.UUID;

public final class BanEntry {

    private final UUID banningUniqueId;
    private UUID unbanningUniqueId;
    private final int banDate;
    private int unbanDate;
    private final int expiry;
    private final String reason;

    public BanEntry(UUID banningUniqueId, int banDate, int expiry, String reason) {
        this(banningUniqueId, banDate, expiry, reason, null, -1);
    }

    public BanEntry(UUID banningUniqueId, int banDate, int expiry, String reason, UUID unbanningUniqueId,
                    int unbanDate) {
        this.banningUniqueId = banningUniqueId;
        this.banDate = banDate;
        this.expiry = expiry;
        this.reason = reason;
        this.unbanningUniqueId = unbanningUniqueId;
        this.unbanDate = unbanDate;
    }

    public UUID getBanningUniqueId() {
        return banningUniqueId;
    }

    public UUID getUnbanningUniqueId() {
        return unbanningUniqueId;
    }

    public int getBanDate() {
        return banDate;
    }

    public int getUnbanDate() {
        return unbanDate;
    }

    public int getExpiry() {
        return expiry;
    }

    public String getReason() {
        return reason;
    }

    public void unban(UUID unbanningUniqueId) {
        if (this.unbanningUniqueId != null) {
            return;
        }
        this.unbanningUniqueId = unbanningUniqueId;
        this.unbanDate = Util.unixTimestamp();
    }

    public boolean isPermanent() {
        return expiry == -1;
    }

    public boolean isValid() {
        return (expiry == -1 || expiry > Util.unixTimestamp()) && unbanningUniqueId == null;
    }
}