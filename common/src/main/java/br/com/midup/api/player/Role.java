package br.com.midup.api.player;

import br.com.midup.util.ColoredName;

import java.util.Set;

public interface Role extends ColoredName {

    public int getId();

    public String getName();

    public String getColor();

    public Set<String> getAliases();
}