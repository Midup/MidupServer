package br.com.midup.api.punishment;

import br.com.midup.util.Util;

import java.util.UUID;

public final class MuteEntry {

    private final UUID mutingUniqueId;
    private String cachedMutingName;
    private UUID unmutingUniqueId;
    private final int muteDate;
    private int unmuteDate;
    private final int expiry;
    private final String reason;

    public MuteEntry(UUID mutingUniqueId, int muteDate, int expiry, String reason) {
        this(mutingUniqueId, muteDate, expiry, reason, null, -1);
    }

    public MuteEntry(UUID mutingUniqueId, int banDate, int expiry, String reason, UUID unmutingUniqueId,
                     int unmuteDate) {
        this.mutingUniqueId = mutingUniqueId;
        this.cachedMutingName = null;
        this.muteDate = banDate;
        this.expiry = expiry;
        this.reason = reason;
        this.unmutingUniqueId = unmutingUniqueId;
        this.unmuteDate = unmuteDate;
    }

    public void setCachedMutingName(String name) {
        this.cachedMutingName = name;
    }

    public String getCachedMutingName() {
        return cachedMutingName;
    }

    public UUID getMutingUniqueId() {
        return mutingUniqueId;
    }

    public UUID getUnmutingUniqueId() {
        return unmutingUniqueId;
    }

    public int getMuteDate() {
        return muteDate;
    }

    public int getUnmuteDate() {
        return unmuteDate;
    }

    public int getExpiry() {
        return expiry;
    }

    public String getReason() {
        return reason;
    }

    public void unmute(UUID unmutingUniqueId) {
        if (this.unmutingUniqueId != null) {
            return;
        }
        this.unmutingUniqueId = unmutingUniqueId;
        this.unmuteDate = Util.unixTimestamp();
    }

    public boolean isPermanent() {
        return expiry == -1;
    }

    public boolean isValid() {
        return (expiry == -1 || expiry > Util.unixTimestamp()) && unmutingUniqueId == null;
    }
}