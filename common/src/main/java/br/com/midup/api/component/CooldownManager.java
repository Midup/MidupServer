package br.com.midup.api.component;

import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.common.player.BasePlayer;

import java.util.*;

public class CooldownManager<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role> {

    protected MidupCommon<TPlayer, TData, TRole> plugin;

    private Map<String, Map<UUID, Long>> cooldowns;
    private Map<String, List<UUID>> waits;

    public CooldownManager(MidupCommon<TPlayer, TData, TRole> plugin) {
        this.plugin = plugin;
        this.cooldowns = new HashMap<>();
        this.waits = new HashMap<>();
    }

    public void registerCooldownCategory(String category) {
        Map<UUID, Long> newCategory = new HashMap<>();
        cooldowns.put(category.toLowerCase(), newCategory);
        for (TPlayer tPlayer : plugin.getPlayerList().getPlayers()) {
            newCategory.put(tPlayer.getData().getUniqueId(), 0L);
        }
    }

    public void registerWaitCategory(String category) {
        List<UUID> newCategory = new ArrayList<>();
        waits.put(category.toLowerCase(), newCategory);
    }

    public void unregisterCooldownCategory(String category) {
        ((Map) cooldowns.remove(category.toLowerCase())).clear();
    }

    public void unregisterWaitCategory(String category) {
        ((List) waits.remove(category.toLowerCase())).clear();
    }

    public void addCooldown(String category, TPlayer tPlayer, long cooldown) {
        addCooldown(category, tPlayer.getData().getUniqueId(), cooldown);
    }

    public void addCooldown(String category, UUID uniqueId, long cooldown) {
        cooldowns.get(category.toLowerCase()).put(uniqueId,
                System.currentTimeMillis() + cooldown);
    }

    public void removeCooldown(String category, TPlayer tPlayer) {
        removeCooldown(category, tPlayer.getData().getUniqueId());
    }

    public void removeCooldown(String category, UUID uniqueId) {
        ((Map) cooldowns.get(category.toLowerCase())).remove(uniqueId);
    }

    public boolean hasCooldown(String category, TPlayer tPlayer) {
        return hasCooldown(category, tPlayer.getData().getUniqueId());
    }

    public boolean hasCooldown(String category, UUID uniqueId) {
        Map cooldown = (Map) cooldowns.get(category.toLowerCase());
        return (cooldown.containsKey(uniqueId))
                && ((Long) cooldown.get(uniqueId) > System.currentTimeMillis());
    }

    public void setWait(String category, TPlayer tPlayer) {
        setWait(category, tPlayer.getData().getUniqueId());
    }

    public void setWait(String category, UUID uniqueId) {
        waits.get(category).add(uniqueId);
    }

    public void removeWait(String category, TPlayer tPlayer) {
        removeWait(category, tPlayer.getData().getUniqueId());
    }

    public void removeWait(String category, UUID uniqueId) {
        ((List) waits.get(category)).remove(uniqueId);
    }

    public boolean isWaiting(String category, TPlayer tPlayer) {
        return isWaiting(category, tPlayer.getData().getUniqueId());
    }

    public boolean isWaiting(String category, UUID uniqueId) {
        return ((List) waits.get(category)).contains(uniqueId);
    }

    public void registerPlayer(TPlayer tPlayer) {
        UUID uniqueId = tPlayer.getData().getUniqueId();
        long now = System.currentTimeMillis();
        for (String category : cooldowns.keySet()) {
            cooldowns.get(category).put(uniqueId, now);
        }
    }

    public void unregisterPlayer(TPlayer tPlayer) {
        UUID uniqueId = tPlayer.getData().getUniqueId();
        for (String category : cooldowns.keySet()) {
            ((Map) cooldowns.get(category)).remove(uniqueId);
        }
        for (String category : waits.keySet()) {
            ((List) waits.get(category)).remove(uniqueId);
        }
    }

}