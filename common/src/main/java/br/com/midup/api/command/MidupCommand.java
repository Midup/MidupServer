package br.com.midup.api.command;

import br.com.midup.api.player.Role;
import br.com.midup.util.ColorUtil;
import org.bukkit.command.CommandSender;

public abstract class MidupCommand {

    protected String name;
    protected final String syntax;
    protected Role neededRole;
    protected final String permission;

    public MidupCommand(String name, Role neededRole, String permission, String syntax) {
        this.name = name;
        this.neededRole = neededRole;
        this.permission = permission;
        this.syntax = syntax;
    }

    public String getName() {
        return name;
    }

    public String getSyntax() {
        return ColorUtil.RED + "Syntax: " + syntax;
    }

    public Role getNeededRole() {
        return neededRole;
    }

    public String getPermission() {
        return permission;
    }

    public abstract void execute(CommandSender sender, String label, String[] args);

}
