package br.com.midup.api.command;

import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.common.logging.FormattedLogger;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.util.ColorUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class MidupCommandManager<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role> {

    private MidupCommon<TPlayer, TData, TRole> plugin;

    private Map<String, MidupCommand> commands;

    private FormattedLogger logger;

    private static final String INEXISTENT_COMMAND = ColorUtil.GRAY + "Esse comando ainda não foi registrado.";
    private static final String PERMISSION_DENIED = ColorUtil.GRAY + "Você não tem permissão para usar esse comando.";
    private static final String INVALID_ROLE = ColorUtil.GRAY + "Você só pode usar esse comando se estiver " + ColorUtil.YELLOW + "%s" + ColorUtil.GRAY + ".";

    public MidupCommandManager(MidupCommon<TPlayer, TData, TRole> plugin) {
        this.plugin = plugin;
        this.commands = new HashMap<>();
        this.logger = new FormattedLogger(plugin.getParentLogger(), "Command");
    }

    public void registerCommand(MidupCommand command) {
        String cmdName = command.getName().toLowerCase();
        commands.put(cmdName, command);
        logger.log(Level.INFO, "Command registered: " + cmdName);
    }

    public void unregisterCommand(MidupCommand command) {
        String cmdName = command.getName().toLowerCase();
        commands.remove(cmdName);
        logger.log(Level.INFO, "Command unregistered: " + cmdName);
    }

    public MidupCommand getCommand(String commandName) {
        String cmdName = commandName.toLowerCase();
        if (commands.containsKey(cmdName)) {
            return (MidupCommand) commands.get(cmdName);
        }
        return null;
    }

    public void handleCommand(CommandSender sender, Command command, String label, String[] args) {
        MidupCommand cCommand = (MidupCommand) getCommand(command.getName());

        if (cCommand == null) {
            sender.sendMessage(INEXISTENT_COMMAND);
            return;
        }

        if (sender instanceof Player) {
            TPlayer tPlayer = (TPlayer) plugin.getPlayerList().getPlayer(((Player) sender));
            if (!tPlayer.getPlayer().hasPermission(cCommand.getPermission())) {
                sender.sendMessage(PERMISSION_DENIED);
                return;
            }
            if ((cCommand.getNeededRole() != null) && (tPlayer.getRole() != cCommand.getNeededRole())) {
                sender.sendMessage(String.format(INVALID_ROLE, cCommand.getNeededRole()));
                return;
            }
        }

        cCommand.execute(sender, label, args);
    }
}
