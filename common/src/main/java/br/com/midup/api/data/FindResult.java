package br.com.midup.api.data;

import br.com.midup.api.player.Role;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.common.player.BasePlayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class FindResult<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role> {

    private final Result result;
    private final List<TData> dataFound;

    public FindResult(Result result) {
        this(result, new ArrayList<>());
    }

    public FindResult(Result result, List<TData> dataFound) {
        this.result = result;
        this.dataFound = Collections.unmodifiableList(dataFound);
    }

    public Result getResult() {
        return result;
    }

    public TData getFirstDataFound() {
        return dataFound.size() < 1 ? null : dataFound.get(0);
    }

    public List<TData> getDataFound() {
        return dataFound;
    }

    public static enum Result {
        FOUND_ONE,
        FOUND_MULTIPLE,
        NOT_FOUND,
        ERROR;
    }
}