package br.com.midup.common.command;

import static br.com.midup.api.data.DataField.DataFieldOption.*;
import static br.com.midup.util.Messages.*;

import br.com.midup.api.command.MidupCommand;
import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.api.data.DataField;
import br.com.midup.api.data.FindResult;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.api.punishment.BanEntry;
import br.com.midup.util.ColorUtil;
import br.com.midup.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class CommandUnban<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role>
        extends MidupCommand {

    private static final String PLAYER_UNBANNED = "%s" + ColorUtil.RED + " has been unbanned!";

    private static final String NOT_BANNED = ColorUtil.RED + "The player '%s' is not banned!";

    private MidupCommon<TPlayer, TData, TRole> plugin;

    public CommandUnban(MidupCommon<TPlayer, TData, TRole> plugin) {
        super("unban", null, "midup.command.unban", "/%s <player/uuid>");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String label, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(String.format(getSyntax(), name));
            return;
        }

        UUID uniqueId = Util.uniqueId(args[0]);
        Player player;

        if (uniqueId == null) {
            player = Bukkit.getPlayer(args[0]);
        } else {
            player = Bukkit.getPlayer(uniqueId);
        }

        if (player != null) {
            sender.sendMessage(String.format(NOT_BANNED, player.getName()));
            return;
        }

        UUID unbanningUniqueId = Util.CONSOLE_UUID;
        if (sender instanceof Player) {
            unbanningUniqueId = ((Player) sender).getUniqueId();
        }

        Runnable groupTask = new AsyncUnbanTask(sender, unbanningUniqueId, args[0]);
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, groupTask);
        return;
    }

    private class AsyncUnbanTask implements Runnable {

        private final CommandSender sender;
        private final UUID unbanningUniqueId;
        private final String targetPlayerName;

        private AsyncUnbanTask(CommandSender sender, UUID unbanningUniqueId, String targetPlayerName) {
            this.sender = sender;
            this.unbanningUniqueId = unbanningUniqueId;
            this.targetPlayerName = targetPlayerName;
        }

        @Override
        public void run() {
            UUID uniqueId = Util.uniqueId(targetPlayerName);
            FindResult<TPlayer, TData, TRole> findResult;

            if (uniqueId == null) {
                findResult = plugin.getDataHandler().findDataByName(targetPlayerName, DataField.of(NAME, GROUP, BANS),
                        false);
            } else {
                findResult = plugin.getDataHandler().findDataByUniqueId(uniqueId, DataField.of(NAME, GROUP, BANS),
                        false);
            }

            FindResult.Result result = findResult.getResult();
            if (result == FindResult.Result.NOT_FOUND) {
                sender.sendMessage(
                        String.format(uniqueId == null ? PLAYER_NOT_IN_DB : UUID_NOT_IN_DB, targetPlayerName));
                return;
            } else if (result == FindResult.Result.ERROR) {
                sender.sendMessage(PLAYER_DATA_FIND_ERROR);
                return;
            } else if (result == FindResult.Result.FOUND_MULTIPLE) {
                sender.sendMessage(String.format(MULTIPLE_FOUND, targetPlayerName));
                return;
            }

            TData playerData = findResult.getFirstDataFound();
            BanEntry currentBan = playerData.getFirstValidBanEntry();
            if (currentBan == null) {
                sender.sendMessage(String.format(NOT_BANNED, playerData.getName()));
                return;
            }

            currentBan.unban(unbanningUniqueId);

            if (!plugin.getDataHandler().updateData(playerData, DataField.of(BANS), false)) {
                sender.sendMessage(PLAYER_DATA_UPDATE_ERROR);
                return;
            }

            sender.sendMessage(String.format(PLAYER_UNBANNED, playerData.getGroup().getColor() + playerData.getName()));
        }
    }
}