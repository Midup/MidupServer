package br.com.midup.common.data;

import br.com.midup.api.player.Group;
import br.com.midup.api.punishment.BanEntry;
import br.com.midup.api.punishment.MuteEntry;
import br.com.midup.common.networking.ServerType;
import br.com.midup.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class BasePlayerData {

    private final UUID uniqueId;
    private final String name;
    protected Group group;
    protected int groupExpiry;
    protected final List<BanEntry> bans;
    protected final List<MuteEntry> mutes;

    protected BasePlayerData(UUID uniqueId, String name) {
        this.uniqueId = uniqueId;
        this.name = name;
        this.group = Group.getDefaultGroup();
        this.groupExpiry = -1;
        this.bans = new ArrayList<>();
        this.mutes = new ArrayList<>();
    }

    public final UUID getUniqueId() {
        return uniqueId;
    }

    public final String getName() {
        return name;
    }

    public final Group getGroup() {
        return group;
    }

    public final void setGroup(Group group) {
        this.group = group;
    }

    public final int getGroupExpiry() {
        return groupExpiry;
    }

    public final void setGroupExpiry(int groupExpiry) {
        this.groupExpiry = groupExpiry;
    }

    public final boolean isGroupExpired() {
        return groupExpiry != -1 && groupExpiry < Util.unixTimestamp();
    }

    public final boolean isGroupPermanent() {
        return groupExpiry == -1;
    }

    public final List<BanEntry> getBans() {
        return bans;
    }

    public final List<MuteEntry> getMutes() {
        return mutes;
    }

    public final BanEntry getFirstValidBanEntry() {
        for (BanEntry entry : bans) {
            if (entry.isValid()) {
                return entry;
            }
        }
        return null;
    }

    public final MuteEntry getFirstValidMuteEntry() {
        for (MuteEntry entry : mutes) {
            if (entry.isValid()) {
                return entry;
            }
        }
        return null;
    }
}