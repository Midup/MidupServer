package br.com.midup.common.listener;

import static br.com.midup.api.data.DataField.DataFieldOption.GROUP;
import static br.com.midup.api.data.DataField.DataFieldOption.NAME;
import static br.com.midup.util.Messages.UNKNOWN;

import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.api.data.DataField;
import br.com.midup.api.data.FindResult;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.api.punishment.MuteEntry;
import br.com.midup.util.ColorUtil;
import br.com.midup.util.Util;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public abstract class BaseChatListener<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role>
        implements Listener {

    private static final String YOU_ARE_MUTED = ColorUtil.BOLD_C + "You're muted!" + ColorUtil.RED + "\nMuted by: %s"
            + ColorUtil.RED + "\nWhen: " + ColorUtil.RESET + "%s" + ColorUtil.RED + "\nReason: " + ColorUtil.RESET
            + "%s";

    private static final String YOU_ARE_TEMP_MUTED = ColorUtil.BOLD_C + "You're temporarily muted!" + ColorUtil.RED
            + "\nMuted by: %s" + ColorUtil.RED + "\nWhen: " + ColorUtil.RESET + "%s" + ColorUtil.RED + "\nReason: "
            + ColorUtil.RESET + "%s" + ColorUtil.RED + "\nExpires in: " + ColorUtil.RESET + "%s";

    protected MidupCommon<TPlayer, TData, TRole> plugin;

    public BaseChatListener(MidupCommon<TPlayer, TData, TRole> plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public final void onBaseChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        TPlayer tPlayer = plugin.getPlayerList().getPlayer(player);
        MuteEntry entry = tPlayer.getCurrentMute();

        if (entry != null && entry.isValid()) {
            String when = Util.getLongTimeString(Util.unixTimestamp() - entry.getMuteDate()) + " ago";
            if (entry.getCachedMutingName() == null) {
                if (entry.getMutingUniqueId().equals(Util.CONSOLE_UUID)) {
                    entry.setCachedMutingName(Util.CONSOLE_NAME);
                } else {
                    FindResult<TPlayer, TData, TRole> findResult = plugin.getDataHandler()
                            .findDataByUniqueId(entry.getMutingUniqueId(), DataField.of(NAME, GROUP), false);
                    FindResult.Result result = findResult.getResult();
                    if (result == FindResult.Result.FOUND_ONE || result == FindResult.Result.FOUND_MULTIPLE) {
                        TData playerData = findResult.getFirstDataFound();
                        entry.setCachedMutingName(playerData.getGroup().getColor() + playerData.getName());
                    } else {
                        entry.setCachedMutingName(UNKNOWN);
                    }
                }
            }
            player.sendMessage(entry.isPermanent()
                    ? String.format(YOU_ARE_MUTED, entry.getCachedMutingName(), when, entry.getReason())
                    : String.format(YOU_ARE_TEMP_MUTED, entry.getCachedMutingName(), when, entry.getReason(),
                    Util.getLongTimeString(entry.getExpiry() - Util.unixTimestamp())));
            event.setCancelled(true);
            return;
        }
    }
}