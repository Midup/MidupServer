package br.com.midup.common.networking.packet.in;

import br.com.midup.common.networking.packet.PacketHandler;
import br.com.midup.common.networking.packet.PacketIn;
import br.com.midup.common.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataInput;

public class PacketInDisconnect extends PacketIn {

    private String reason;

    public PacketInDisconnect() {
        super(PacketType.In.DISCONNECT);
    }

    public String getReason() {
        return reason;
    }

    @Override
    public void readPacketData(ByteArrayDataInput input) {
        this.reason = input.readUTF();
    }

    @Override
    public void processPacketData(PacketHandler<?, ?, ?> handler) {
        handler.processDisconnect(this);
    }
}