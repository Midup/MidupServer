package br.com.midup.common.player;

import br.com.midup.api.player.Group;
import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.api.punishment.MuteEntry;
import br.com.midup.common.reflection.PermissionReflection;
import br.com.midup.util.ColorUtil;
import br.com.midup.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionRemovedExecutor;

import java.util.*;

public final class Permissions<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role> {

    private static final String TEMP_GROUP_PERM = "midup.group";
    private static final String TEMP_MUTE_PERM = "midup.mute";
    private static final String GROUP_EXPIRED = ColorUtil.BOLD_C + "Your %s" + ColorUtil.BOLD_C + " has expired!";
    private static final String MUTE_EXPIRED = ColorUtil.BOLD_A
            + "Your mute has expired! You can use the chat once again!";

    private final MidupCommon<TPlayer, TData, TRole> plugin;

    private final Map<Group, Map<TRole, Map<String, Boolean>>> groupPermissions;
    private final Map<UUID, PermissionAttachment> permissionAttachments;
    private final Map<UUID, PermissionAttachment> groupAttachments;
    private final Map<UUID, PermissionAttachment> muteAttachments;

    public Permissions(MidupCommon<TPlayer, TData, TRole> plugin) {
        this.plugin = plugin;
        this.groupPermissions = new EnumMap<>(Group.class);
        this.permissionAttachments = new HashMap<>();
        this.groupAttachments = new HashMap<>();
        this.muteAttachments = new HashMap<>();
    }

    public void initialize() {
        for (Group group : Group.values()) {
            Map<TRole, Map<String, Boolean>> rolePermissions = new HashMap<>();
            for (TRole role : plugin.getPlayerList().getSpecificRoles()) {
                rolePermissions.put(role, new HashMap<>());
            }
            groupPermissions.put(group, rolePermissions);
        }
    }

    public void registerPlayer(TPlayer tPlayer) {
        Player player = tPlayer.getPlayer();
        PermissionAttachment attachment = player.addAttachment(plugin);
        permissionAttachments.put(player.getUniqueId(), attachment);
        this.updatePlayer(tPlayer, true, true, true);
    }

    public void updatePlayer(TPlayer tPlayer, boolean updatePermissions, boolean updateTempGroup,
                             boolean updateTempMute) {
        Player player = tPlayer.getPlayer();
        TData playerData = tPlayer.getData();
        if (updatePermissions) {
            PermissionAttachment permissionAttachment = permissionAttachments.get(player.getUniqueId());
            if (permissionAttachment != null) {
                Map<String, Boolean> permissions = groupPermissions.get(playerData.getGroup()).get(tPlayer.getRole());
                PermissionReflection.setPermissions(permissionAttachment, permissions, true);
            }
        }
        if (updateTempGroup && !playerData.isGroupPermanent()) {
            PermissionAttachment groupAttachment = groupAttachments.get(player.getUniqueId());
            if (groupAttachment != null) {
                groupAttachment.setRemovalCallback(null);
                groupAttachment.remove();
                groupAttachments.remove(groupAttachment);
            }

            int secondsLeft = playerData.getGroupExpiry() - Util.unixTimestamp();
            if (secondsLeft < 1) {
                player.sendMessage(String.format(GROUP_EXPIRED, playerData.getGroup().getColoredName()));
                player.playSound(player.getLocation(), Sound.BLAZE_DEATH, 1.0F, 1.0F);
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
                        "group " + player.getUniqueId() + " " + Group.getDefaultGroup().getName());
                return;
            }
            groupAttachment = player.addAttachment(plugin, TEMP_GROUP_PERM, true, secondsLeft * 20);
            groupAttachment.setRemovalCallback(new TemporaryGroupHandler());
        }
        if (updateTempMute) {
            MuteEntry entry = playerData.getFirstValidMuteEntry();
            tPlayer.setCurrentMute(entry != null && entry.isValid() ? entry : null);

            PermissionAttachment muteAttachment = muteAttachments.get(player.getUniqueId());
            if (muteAttachment != null) {
                muteAttachment.setRemovalCallback(null);
                muteAttachment.remove();
            }
            if (entry != null && !entry.isPermanent() && entry.isValid()) {
                int secondsLeft = entry.getExpiry() - Util.unixTimestamp();
                if (secondsLeft > 0) {
                    muteAttachment = player.addAttachment(plugin, TEMP_MUTE_PERM, true, secondsLeft * 20);
                    muteAttachment.setRemovalCallback(new TemporaryMuteHandler());
                }
            }
        }
    }

    public void unregisterPlayer(TPlayer tPlayer) {
        UUID uniqueId = tPlayer.getPlayer().getUniqueId();
        PermissionAttachment permissionAttachment = permissionAttachments.remove(uniqueId);
        if (permissionAttachment != null) {
            permissionAttachment.remove();
        }

        PermissionAttachment groupAttachment = groupAttachments.remove(uniqueId);
        if (groupAttachment != null) {
            groupAttachment.remove();
        }

    }

    public void setPermission(Group group, Role role, String permission, boolean value) {
        this.groupPermissions.get(group).get(role).put(permission, value);
        for (TPlayer tPlayer : plugin.getPlayerList().getPlayersByGroupRole(group, role)) {
            PermissionAttachment attachment = this.permissionAttachments.get(tPlayer.getPlayer().getUniqueId());
            attachment.setPermission(permission, value);
        }
    }

    public void unsetPermission(Group group, Role role, String permission) {
        this.groupPermissions.get(group).get(role).remove(permission);
        for (TPlayer tPlayer : plugin.getPlayerList().getPlayersByGroupRole(group, role)) {
            PermissionAttachment attachment = this.permissionAttachments.get(tPlayer.getPlayer().getUniqueId());
            attachment.unsetPermission(permission);
        }
    }

    public void setPermissions(Group group, Role role, Map<String, Boolean> permissions) {
        this.groupPermissions.get(group).get(role).putAll(permissions);
        for (TPlayer tPlayer : plugin.getPlayerList().getPlayersByGroupRole(group, role)) {
            PermissionAttachment attachment = this.permissionAttachments.get(tPlayer.getPlayer().getUniqueId());
            PermissionReflection.setPermissions(attachment, permissions, false);
        }
    }

    public void unsetPermissions(Group group, Role role, Set<String> permissions) {
        Util.removeAll(this.groupPermissions.get(group).get(role), permissions);
        for (TPlayer tPlayer : plugin.getPlayerList().getPlayersByGroupRole(group, role)) {
            PermissionAttachment attachment = this.permissionAttachments.get(tPlayer.getPlayer().getUniqueId());
            PermissionReflection.unsetPermissions(attachment, permissions);
        }
    }

    private class TemporaryGroupHandler implements PermissionRemovedExecutor {

        @Override
        public void attachmentRemoved(PermissionAttachment attachment) {
            if (attachment.getPermissions().size() < 1) {
                return;
            }

            if (!attachment.getPermissions().keySet().iterator().next().equals(TEMP_GROUP_PERM)) {
                return;
            }

            if (!(attachment.getPermissible() instanceof Player)) {
                return;
            }

            Player player = (Player) attachment.getPermissible();
            TData playerData = plugin.getPlayerList().getPlayer(player).getData();
            player.sendMessage(String.format(GROUP_EXPIRED, playerData.getGroup().getColoredName()));
            player.playSound(player.getLocation(), Sound.BLAZE_DEATH, 1.0F, 1.0F);
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
                    "group " + player.getUniqueId() + " " + Group.getDefaultGroup().getName());
        }
    }

    private class TemporaryMuteHandler implements PermissionRemovedExecutor {

        @Override
        public void attachmentRemoved(PermissionAttachment attachment) {
            if (attachment.getPermissions().size() < 1) {
                return;
            }

            if (!attachment.getPermissions().keySet().iterator().next().equals(TEMP_MUTE_PERM)) {
                return;
            }

            if (!(attachment.getPermissible() instanceof Player)) {
                return;
            }

            Player player = (Player) attachment.getPermissible();
            TPlayer tPlayer = plugin.getPlayerList().getPlayer(player);
            tPlayer.setCurrentMute(null);
            player.sendMessage(MUTE_EXPIRED);
            player.playSound(player.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
        }
    }
}