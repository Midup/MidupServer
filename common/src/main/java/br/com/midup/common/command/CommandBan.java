package br.com.midup.common.command;

import static br.com.midup.api.data.DataField.DataFieldOption.*;
import static br.com.midup.util.Messages.*;

import br.com.midup.api.command.MidupCommand;
import br.com.midup.api.player.Group;
import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.api.data.DataField;
import br.com.midup.api.data.FindResult;
import br.com.midup.common.networking.packet.out.PacketOutBanRequest;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.api.punishment.BanEntry;
import br.com.midup.util.ColorUtil;
import br.com.midup.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.UUID;

public class CommandBan<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role>
        extends MidupCommand {

    private static final String PLAYER_BANNED = "%s" + ColorUtil.RED + " has been banned!";

    private MidupCommon<TPlayer, TData, TRole> plugin;

    public CommandBan(MidupCommon<TPlayer, TData, TRole> plugin) {
        super("ban", null, "midup.command.ban", "/%s <player/uuid> <reason...>");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String label, String[] args) {
        if (args.length < 2) {
            sender.sendMessage(String.format(getSyntax(), name));
            return;
        }

        String[] reasonArray = Arrays.copyOfRange(args, 1, args.length);
        String reason = Util.stringJoin(reasonArray, " ");

        UUID uniqueId = Util.uniqueId(args[0]);
        Player player;

        if (uniqueId == null) {
            player = Bukkit.getPlayer(args[0]);
        } else {
            player = Bukkit.getPlayer(uniqueId);
        }

        if (player == null) {
            sender.sendMessage(String
                    .format(uniqueId == null ? PLAYER_NOT_FOUND_SEARCHING_DB : UUID_NOT_FOUND_SEARCHING_DB, args[0]));

            Runnable groupTask = new AsyncBanTask(sender, args[0], reason);
            plugin.getServer().getScheduler().runTaskAsynchronously(plugin, groupTask);
            return;
        } else if (player.equals(sender)) {
            sender.sendMessage(CANNOT_SELF_BAN);
            return;
        }

        TPlayer tPlayer = plugin.getPlayerList().getPlayer(player);
        TData tData = tPlayer.getData();
        if (tData.getGroup().isStaff()) {
            sender.sendMessage(CANNOT_BAN_STAFF);
            return;
        }

        UUID banningUniqueId = sender instanceof Player ? ((Player) sender).getUniqueId() : Util.CONSOLE_UUID;
        tData.getBans().add(new BanEntry(banningUniqueId, Util.unixTimestamp(), -1, reason));
        sender.sendMessage(String.format(PLAYER_BANNED, player.getDisplayName()));
        player.kickPlayer(String.format(YOU_HAVE_BEEN_BANNED, Util.getDisplayName(sender), reason));
        return;
    }

    private class AsyncBanTask implements Runnable {

        private final CommandSender sender;
        private final String targetPlayerName;
        private final String reason;

        private AsyncBanTask(CommandSender sender, String targetPlayerName, String reason) {
            this.sender = sender;
            this.targetPlayerName = targetPlayerName;
            this.reason = reason;
        }

        @Override
        public void run() {
            UUID uniqueId = Util.uniqueId(targetPlayerName);
            FindResult<TPlayer, TData, TRole> findResult;

            if (uniqueId == null) {
                findResult = plugin.getDataHandler().findDataByName(targetPlayerName, DataField.of(NAME, GROUP, BANS),
                        false);
            } else {
                findResult = plugin.getDataHandler().findDataByUniqueId(uniqueId, DataField.of(NAME, GROUP, BANS),
                        false);
            }

            FindResult.Result result = findResult.getResult();
            if (result == FindResult.Result.NOT_FOUND) {
                sender.sendMessage(
                        String.format(uniqueId == null ? PLAYER_NOT_IN_DB : UUID_NOT_IN_DB, targetPlayerName));
                return;
            } else if (result == FindResult.Result.ERROR) {
                sender.sendMessage(PLAYER_DATA_FIND_ERROR);
                return;
            } else if (result == FindResult.Result.FOUND_MULTIPLE) {
                sender.sendMessage(String.format(MULTIPLE_FOUND, targetPlayerName));
                return;
            }

            TData playerData = findResult.getFirstDataFound();
            if (playerData.getFirstValidBanEntry() != null) {
                sender.sendMessage(String.format(ALREADY_BANNED, playerData.getName()));
                return;
            }

            Group group = playerData.getGroup();
            if (group.isStaff()) {
                sender.sendMessage(CANNOT_BAN_STAFF);
                return;
            }

            UUID banningUniqueId = sender instanceof Player ? ((Player) sender).getUniqueId() : Util.CONSOLE_UUID;
            String banningName = sender instanceof Player ? ((Player) sender).getDisplayName() : Util.CONSOLE_NAME;
            playerData.getBans().add(new BanEntry(banningUniqueId, Util.unixTimestamp(), -1, reason));

            if (!plugin.getDataHandler().updateData(playerData, DataField.of(BANS), false)) {
                sender.sendMessage(PLAYER_DATA_UPDATE_ERROR);
                return;
            }
            plugin.getProxyConnection()
                    .sendPacket(new PacketOutBanRequest(playerData.getUniqueId(), banningUniqueId, banningName, reason, -1));

            sender.sendMessage(String.format(PLAYER_BANNED, group.getColor() + playerData.getName()));
        }
    }
}