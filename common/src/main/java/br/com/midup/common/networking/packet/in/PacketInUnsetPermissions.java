package br.com.midup.common.networking.packet.in;

import br.com.midup.api.player.Group;
import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.networking.packet.PacketHandler;
import br.com.midup.common.networking.packet.PacketIn;
import br.com.midup.common.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataInput;

import java.util.LinkedHashSet;
import java.util.Set;

public class PacketInUnsetPermissions extends PacketIn {

    private Group group;
    private Role role;

    private Set<String> permissions;

    public PacketInUnsetPermissions() {
        super(PacketType.In.UNSET_PERMISSIONS);
        this.permissions = new LinkedHashSet<>();
    }

    public Group getGroup() {
        return group;
    }

    public Role getRole() {
        return role;
    }

    public int getAmount() {
        return permissions.size();
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    @Override
    public void readPacketData(ByteArrayDataInput input) {
        int groupId = input.readInt();
        this.group = Group.getGroup(groupId);
        if (group == null) {
            throw new IllegalArgumentException("Invalid group id: " + groupId);
        }

        int roleId = input.readInt();
        this.role = MidupCommon.getInstance().getPlayerList().getSpecificRole(roleId);
        if (role == null) {
            throw new IllegalArgumentException("Invalid role id: " + roleId);
        }

        int unsetPermissions = input.readInt();
        for (int i = 0; i < unsetPermissions; i++) {
            this.permissions.add(input.readUTF());
        }
    }

    @Override
    public void processPacketData(PacketHandler<?, ?, ?> handler) {
        handler.processUnsetPermissions(this);
    }
}