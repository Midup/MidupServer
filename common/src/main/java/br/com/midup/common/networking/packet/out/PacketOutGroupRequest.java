package br.com.midup.common.networking.packet.out;

import br.com.midup.api.player.Group;
import br.com.midup.common.networking.packet.PacketOut;
import br.com.midup.common.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataOutput;

import java.util.UUID;

public class PacketOutGroupRequest extends PacketOut {

    private final UUID uniqueId;
    private final Group group;
    private final int duration;

    public PacketOutGroupRequest(UUID uniqueId, Group group, int duration) {
        super(PacketType.Out.GROUP_REQUEST);
        this.uniqueId = uniqueId;
        this.group = group;
        this.duration = duration;
    }

    @Override
    public void writePacketData(ByteArrayDataOutput output) {
        output.writeLong(uniqueId.getMostSignificantBits());
        output.writeLong(uniqueId.getLeastSignificantBits());

        output.writeInt(group.getId());
        output.writeInt(duration);
    }
}