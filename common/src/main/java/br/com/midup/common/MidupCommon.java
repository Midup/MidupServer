package br.com.midup.common;

import br.com.midup.api.command.MidupCommandManager;
import br.com.midup.api.player.Role;
import br.com.midup.common.command.*;
import br.com.midup.common.data.BaseDataHandler;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.common.logging.FormattedLogger;
import br.com.midup.common.networking.ProxyConnection;
import br.com.midup.common.networking.ServerType;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.common.player.BasePlayerList;
import br.com.midup.common.player.Permissions;

import br.com.midup.api.component.CooldownManager;
import br.com.midup.util.Util;
import org.bukkit.command.*;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class MidupCommon<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role>
        extends JavaPlugin {

    private static MidupCommon<?, ?, ?> instance;

    protected final FormattedLogger logger;
    protected final Permissions<TPlayer, TData, TRole> permissions;
    protected final ProxyConnection<TPlayer, TData, TRole> connection;
    protected final MidupCommandManager<TPlayer, TData, TRole> commandManager;
    protected final CooldownManager<TPlayer, TData, TRole> cooldownManager;

    public MidupCommon() {
        instance = this;
        this.logger = new FormattedLogger(getLogger(), null);
        this.permissions = new Permissions<TPlayer, TData, TRole>(this);
        this.connection = new ProxyConnection<TPlayer, TData, TRole>(this, getServerType());
        this.commandManager = new MidupCommandManager<>(this);
        this.cooldownManager = new CooldownManager<TPlayer, TData, TRole>(this);
    }

    public abstract ServerType getServerType();

    public abstract BaseDataHandler<TPlayer, TData, TRole> getDataHandler();

    public abstract BasePlayerList<TPlayer, TData, TRole> getPlayerList();

    @Override
    public void onEnable() {
        super.onEnable();

        logger.log("Starting network module...");
        this.connection.loadConfiguration();
        this.connection.registerReconnectionTask();

        commandManager.registerCommand(new CommandGroup<>(this));
        commandManager.registerCommand(new CommandMute<>(this));
        commandManager.registerCommand(new CommandUnmute<>(this));
        commandManager.registerCommand(new CommandTempmute<>(this));
        commandManager.registerCommand(new CommandBan<>(this));
        commandManager.registerCommand(new CommandTempban<>(this));
        commandManager.registerCommand(new CommandUnban<>(this));

        logger.log("Initializing permission manager with ranks and server specific roles...");
        this.permissions.initialize();

        Util.disableMongoLoggers();
    }

    @Override
    public void onDisable() {
        super.onDisable();

        for (TPlayer tPlayer : this.getPlayerList().getPlayers()) {
            for (int i = 0; i < 100; i++) {
                if (i % 10 == 0) {
                    logger.log("Current in #%d", i);
                }
                this.getDataHandler().updateData(tPlayer.getData());
            }
        }

        logger.log("Stopping network module...");
        this.connection.unregisterReconnectionTask();
        this.connection.disconnect("Server closed!");
    }

    public FormattedLogger getParentLogger() {
        return logger;
    }

    public ProxyConnection<TPlayer, TData, TRole> getProxyConnection() {
        return connection;
    }

    public Permissions<TPlayer, TData, TRole> getPermissions() {
        return permissions;
    }

    public MidupCommandManager<TPlayer, TData, TRole> getCommandManager() {
        return commandManager;
    }

    public CooldownManager<TPlayer, TData, TRole> getCooldownManager() {
        return cooldownManager;
    }

    public static MidupCommon<? extends BasePlayer<? extends BasePlayerData, ? extends Role>, ? extends BasePlayerData, ? extends Role> getInstance() {
        return instance;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        this.commandManager.handleCommand(sender, command, label, args);
        return true;
    }
}
