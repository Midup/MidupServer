package br.com.midup.common.networking.packet;

import br.com.midup.common.networking.packet.in.*;
import br.com.midup.common.networking.packet.out.PacketOutBanRequest;
import br.com.midup.common.networking.packet.out.PacketOutDisconnect;
import br.com.midup.common.networking.packet.out.PacketOutGroupRequest;
import br.com.midup.common.networking.packet.out.PacketOutHandshake;

import java.util.HashMap;
import java.util.Map;

public interface PacketType {

    public byte getId();

    public Class<? extends Packet> getImplClass();

    public static interface PacketTypeIn extends PacketType {

        @Override
        public Class<? extends PacketIn> getImplClass();

        public PacketIn createInstance() throws Exception;

        public boolean isAsync();
    }

    public static interface PacketTypeOut extends PacketType {

        @Override
        public Class<? extends PacketOut> getImplClass();
    }

    public static enum In implements PacketTypeIn {
        ACCEPT_CONNECTION(0, PacketInAcceptConnection.class, true),
        DISCONNECT(1, PacketInDisconnect.class, true),
        SET_PERMISSION(2, PacketInSetPermission.class, false),
        UNSET_PERMISSION(3, PacketInUnsetPermission.class, false),
        SET_PERMISSIONS(4, PacketInSetPermissions.class, false),
        UNSET_PERMISSIONS(5, PacketInUnsetPermissions.class, false),
        GROUP_REQUEST(6, PacketInGroupRequest.class, false),
        BAN_REQUEST(7, PacketInBanRequest.class, false);

        private static final Map<Integer, PacketTypeIn> BY_ID = new HashMap<>();

        private final byte id;
        private final Class<? extends PacketIn> implClass;
        private final boolean async;

        static {
            for (In type : values()) {
                BY_ID.put((int) type.id, type);
            }
        }

        private In(int id, Class<? extends PacketIn> implClass, boolean async) {
            this.id = (byte) id;
            this.implClass = implClass;
            this.async = async;
        }

        @Override
        public byte getId() {
            return id;
        }

        @Override
        public Class<? extends PacketIn> getImplClass() {
            return implClass;
        }

        @Override
        public PacketIn createInstance() throws Exception {
            return implClass.newInstance();
        }

        @Override
        public boolean isAsync() {
            return async;
        }

        public static PacketTypeIn getPacketType(int id) {
            return BY_ID.get(id);
        }
    }

    public static enum Out implements PacketTypeOut {
        HANDSHAKE(0, PacketOutHandshake.class),
        DISCONNECT(1, PacketOutDisconnect.class),
        GROUP_REQUEST(2, PacketOutGroupRequest.class),
        BAN_REQUEST(3, PacketOutBanRequest.class);

        private static final Map<Integer, PacketTypeOut> BY_ID = new HashMap<>();

        private final byte id;
        private final Class<? extends PacketOut> implClass;

        static {
            for (Out type : values()) {
                BY_ID.put((int) type.id, type);
            }
        }

        private Out(int id, Class<? extends PacketOut> implClass) {
            this.id = (byte) id;
            this.implClass = implClass;
        }

        @Override
        public byte getId() {
            return id;
        }

        @Override
        public Class<? extends PacketOut> getImplClass() {
            return implClass;
        }

        public static PacketTypeOut getPacketType(int id) {
            return BY_ID.get(id);
        }
    }
}