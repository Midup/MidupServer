package br.com.midup.common.networking;

import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.common.logging.FormattedLogger;
import br.com.midup.common.networking.packet.PacketHandler;
import br.com.midup.common.networking.packet.PacketOut;
import br.com.midup.common.networking.packet.PacketIn;
import br.com.midup.common.networking.packet.PacketType;
import br.com.midup.common.networking.packet.out.PacketOutDisconnect;
import br.com.midup.common.networking.packet.out.PacketOutHandshake;
import br.com.midup.common.player.BasePlayer;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitTask;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;

public final class ProxyConnection<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role> {

    private static final String CONFIG_FILE_NAME = "connection.yml";

    private final MidupCommon<TPlayer, TData, TRole> plugin;
    private final FormattedLogger logger;
    private final ServerType serverType;
    private final File configFile;
    private final Set<NetworkCallback> registeredCallbacks;
    private BukkitTask reconnectionTask;

    private ConnectionState state;
    private final PacketHandler<TPlayer, TData, TRole> handler;
    private final ReentrantLock connectionLock;

    private FileConfiguration config;
    private String address;
    private int port;

    private Socket socket;
    private DataInputStream input;
    private DataOutputStream output;

    private int connectionRetries;
    private int lastConnectionRetry;

    public ProxyConnection(MidupCommon<TPlayer, TData, TRole> plugin, ServerType serverType) {
        this.plugin = plugin;
        this.logger = new FormattedLogger(plugin.getParentLogger(), "Networking");
        this.serverType = serverType;
        this.configFile = new File(plugin.getDataFolder(), CONFIG_FILE_NAME);
        this.registeredCallbacks = new HashSet<>();

        this.state = ConnectionState.DISCONNECTED;
        this.handler = new PacketHandler<TPlayer, TData, TRole>(plugin, this);
        this.connectionLock = new ReentrantLock();

        this.address = "localhost";
        this.port = 6696;

        this.connectionRetries = 0;
        this.lastConnectionRetry = 0;
    }

    public FormattedLogger getLogger() {
        return logger;
    }

    public ConnectionState getConnectionState() {
        return state;
    }

    public void loadConfiguration() {
        if (!configFile.exists()) {
            plugin.saveResource(CONFIG_FILE_NAME, true);
        }

        this.config = YamlConfiguration.loadConfiguration(configFile);
        if (config.contains("address")) {
            this.address = config.getString("address");
        } else {
            logger.log(Level.WARNING, "Missing 'address' from %s. Using default localhost!", CONFIG_FILE_NAME);
        }
        if (config.contains("port")) {
            this.port = config.getInt("port");
        } else {
            logger.log(Level.WARNING, "Missing 'port' from %s. Using default 6696!", CONFIG_FILE_NAME);
        }
    }

    public void registerReconnectionTask() {
        if (reconnectionTask != null) {
            return;
        }

        logger.log("Starting auto reconnection task...");
        this.reconnectionTask = Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                if (state != ConnectionState.DISCONNECTED) {
                    return;
                }

                if (connectionRetries > 3) {
                    if (lastConnectionRetry < 10) {
                        lastConnectionRetry++;
                        return;
                    }
                    lastConnectionRetry = 0;
                }
                openConnection();
            }
        }, 20, 20);
    }

    public void unregisterReconnectionTask() {
        if (reconnectionTask == null) {
            return;
        }

        logger.log("Stopping auto reconnection task...");
        this.reconnectionTask.cancel();
        this.reconnectionTask = null;
    }

    public void openConnection() {
        if (state != ConnectionState.DISCONNECTED) {
            return;
        }

        updateConnectionState(ConnectionState.CONNECTING);
        logger.log("Preparing to connect to the proxy...");
        try {
            this.socket = new Socket();
            this.socket.connect(new InetSocketAddress(address, port));
            updateConnectionState(ConnectionState.AWAITING_HANDSHAKE);
            this.input = new DataInputStream(socket.getInputStream());
            this.output = new DataOutputStream(socket.getOutputStream());
            new DataReader().start();
            logger.log("Connection with the proxy server opened with success! Sending handshake...");
            sendPacket(new PacketOutHandshake(serverType));
            this.connectionRetries = 0;
        } catch (Exception e) {
            if (this.connectionRetries == 3) {
                logger.log(Level.SEVERE,
                        "3 reconnections failed! Surpressing exceptions and increasing the retry interval!");
            }
            if (this.connectionRetries < 3) {
                logger.log(Level.SEVERE, "Failed to open a connection with the proxy server! Details below:");
                e.printStackTrace();
            } else {
                logger.log(Level.SEVERE, "Failed to open a connection with the proxy server!");
            }
            disconnect("Failed to open connection");
            this.connectionRetries++;
        }
    }

    public void closeConnection() {
        updateConnectionState(ConnectionState.DISCONNECTED);
        connectionLock.lock();
        try {
            socket.close();
        } catch (Exception e) {
        } finally {
            connectionLock.unlock();
        }
    }

    public void updateConnectionState(ConnectionState state) {
        if (this.state == state) {
            return;
        }
        // logger.log("Connection state changed from %s to %s", this.state,
        // state);
        this.state = state;
    }

    public void disconnect(String reason) {
        if (state != ConnectionState.DISCONNECTED) {
            this.sendPacket(new PacketOutDisconnect(reason));
            logger.log("Disconnected! Reason: %s", reason);
        }
        closeConnection();
    }

    public void sendPacket(PacketOut packet) {
        connectionLock.lock();

        try {
            if (state == ConnectionState.DISCONNECTED) {
                return;
            }

            output.writeByte(packet.getPacketType().getId());
            ByteArrayDataOutput packetBody = ByteStreams.newDataOutput();
            packet.writePacketData(packetBody);
            byte[] packetData = packetBody.toByteArray();
            output.writeShort(packetData.length);
            output.write(packetData);
            output.flush();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to send packet to the proxy server! Details below:");
            e.printStackTrace();
        } finally {
            connectionLock.unlock();
        }
    }

    public void registerCallback(NetworkCallback networkCallback) {
        this.registeredCallbacks.add(networkCallback);
    }

    public void unregisterCallback(NetworkCallback networkCallback) {
        this.registeredCallbacks.remove(networkCallback);
    }

    private class DataReader extends Thread {

        @Override
        public void run() {
            while (state != ConnectionState.DISCONNECTED) {
                try {
                    byte packetId = input.readByte();
                    PacketType.PacketTypeIn packetType = PacketType.In.getPacketType(packetId);
                    if (packetType == null) {
                        logger.log(Level.WARNING, "Received an invalid packet mode from the proxy! ID: %d", packetId);
                        disconnect("Invalid packet mode");
                        return;
                    }

                    if (state != ConnectionState.CONNECTED) {
                        if (packetType != PacketType.In.ACCEPT_CONNECTION && packetType != PacketType.In.DISCONNECT) {
                            logger.log(Level.WARNING, "Received a wrong packet while still not authenticated!");
                            disconnect("Illegal packet");
                            return;
                        }
                    } else {
                        if (packetType == PacketType.In.ACCEPT_CONNECTION) {
                            logger.log(Level.WARNING, "Received authentication packet while already authenticated!");
                            disconnect("Illegal packet");
                            return;
                        }
                    }

                    final PacketIn packet = packetType.createInstance();
                    short packetSize = input.readShort();

                    if (packetSize < 0) {
                        logger.log("Received a negative packet size from the proxy! Size: %d", packetSize);
                        disconnect("Invalid packet size");
                        return;
                    }

                    byte[] dataInput = new byte[packetSize];
                    input.readFully(dataInput);
                    packet.readPacketData(ByteStreams.newDataInput(dataInput));

                    if (packet.getPacketType().isAsync()) {
                        for (NetworkCallback networkCallback : registeredCallbacks) {
                            networkCallback.onReceivePacket(packet);
                        }
                        packet.processPacketData(handler);
                    } else {
                        Bukkit.getScheduler().runTask(plugin, new Runnable() {
                            @Override
                            public void run() {
                                for (NetworkCallback networkCallback : registeredCallbacks) {
                                    networkCallback.onReceivePacket(packet);
                                }
                                packet.processPacketData(handler);
                            }
                        });
                    }
                } catch (Exception e) {
                    if (state != ConnectionState.DISCONNECTED) {
                        if (e instanceof SocketException) {
                            updateConnectionState(ConnectionState.DISCONNECTED);
                        }
                        logger.log(Level.SEVERE,
                                "An error occurred while handling incoming data from the proxy! Details below:");
                        e.printStackTrace();
                        disconnect("Data handling error");
                    }
                }
            }

        }
    }
}