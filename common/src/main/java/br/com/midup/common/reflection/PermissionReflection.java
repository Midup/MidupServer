package br.com.midup.common.reflection;

import org.bukkit.permissions.PermissionAttachment;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

public final class PermissionReflection {

    private static final Field F_PERMISSIONS = Reflection.getField(PermissionAttachment.class, "permissions");

    private PermissionReflection() {}

    @SuppressWarnings("unchecked")
    public static void setPermissions(PermissionAttachment attachment, Map<String, Boolean> permissions,
                                      boolean clearOldPermissions) {
        try {
            Map<String, Boolean> currentPermissions = (Map<String, Boolean>) F_PERMISSIONS.get(attachment);
            if (clearOldPermissions) {
                currentPermissions.clear();
            }
            currentPermissions.putAll(permissions);
            attachment.getPermissible().recalculatePermissions();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public static void unsetPermissions(PermissionAttachment attachment, Set<String> permissions) {
        try {
            Map<String, Boolean> currentPermissions = (Map<String, Boolean>) F_PERMISSIONS.get(attachment);
            for (String permission : permissions) {
                currentPermissions.remove(permission);
            }
            attachment.getPermissible().recalculatePermissions();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}