package br.com.midup.common.command;

import static br.com.midup.api.data.DataField.DataFieldOption.*;
import static br.com.midup.util.Messages.*;

import br.com.midup.api.command.MidupCommand;
import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.api.data.DataField;
import br.com.midup.api.data.FindResult;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.api.punishment.MuteEntry;
import br.com.midup.util.ColorUtil;
import br.com.midup.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class CommandUnmute<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role>
        extends MidupCommand {

    private static final String PLAYER_UNMUTED = "%s" + ColorUtil.GREEN + " has been unmuted!";

    private static final String YOU_HAVE_BEEN_UNMUTED = ColorUtil.BOLD_A + "You have been unmuted!";

    private static final String NOT_MUTED = ColorUtil.RED + "The player '%s' is not muted!";

    private MidupCommon<TPlayer, TData, TRole> plugin;

    public CommandUnmute(MidupCommon<TPlayer, TData, TRole> plugin) {
        super("unmute", null, "midup.command.unmute", "/%s <player/uuid>");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String label, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(String.format(getSyntax(), name));
            return;
        }

        UUID uniqueId = Util.uniqueId(args[0]);
        Player player;

        if (uniqueId == null) {
            player = Bukkit.getPlayer(args[0]);
        } else {
            player = Bukkit.getPlayer(uniqueId);
        }

        UUID unmutingUniqueId = Util.CONSOLE_UUID;
        if (sender instanceof Player) {
            unmutingUniqueId = ((Player) sender).getUniqueId();
        }

        if (player == null) {
            sender.sendMessage(String
                    .format(uniqueId == null ? PLAYER_NOT_FOUND_SEARCHING_DB : UUID_NOT_FOUND_SEARCHING_DB, args[0]));

            Runnable groupTask = new AsyncUnmuteTask(sender, unmutingUniqueId, args[0]);
            plugin.getServer().getScheduler().runTaskAsynchronously(plugin, groupTask);
            return;
        }

        TPlayer tPlayer = plugin.getPlayerList().getPlayer(player);
        TData tData = tPlayer.getData();

        MuteEntry currentMute = tData.getFirstValidMuteEntry();
        if (currentMute == null) {
            sender.sendMessage(String.format(NOT_MUTED, player.getName()));
            return;
        }

        currentMute.unmute(unmutingUniqueId);
        sender.sendMessage(String.format(PLAYER_UNMUTED, player.getDisplayName()));
        player.sendMessage(String.format(YOU_HAVE_BEEN_UNMUTED, Util.getDisplayName(sender)));
        plugin.getPermissions().updatePlayer(tPlayer, false, false, true);
    }

    private class AsyncUnmuteTask implements Runnable {

        private final CommandSender sender;
        private final UUID unmutingUniqueId;
        private final String targetPlayerName;

        private AsyncUnmuteTask(CommandSender sender, UUID unmutingUniqueId, String targetPlayerName) {
            this.sender = sender;
            this.unmutingUniqueId = unmutingUniqueId;
            this.targetPlayerName = targetPlayerName;
        }

        @Override
        public void run() {
            UUID uniqueId = Util.uniqueId(targetPlayerName);
            FindResult<TPlayer, TData, TRole> findResult;

            if (uniqueId == null) {
                findResult = plugin.getDataHandler().findDataByName(targetPlayerName, DataField.of(NAME, GROUP, MUTES),
                        false);
            } else {
                findResult = plugin.getDataHandler().findDataByUniqueId(uniqueId, DataField.of(NAME, GROUP, MUTES),
                        false);
            }

            FindResult.Result result = findResult.getResult();
            if (result == FindResult.Result.NOT_FOUND) {
                sender.sendMessage(
                        String.format(uniqueId == null ? PLAYER_NOT_IN_DB : UUID_NOT_IN_DB, targetPlayerName));
                return;
            } else if (result == FindResult.Result.ERROR) {
                sender.sendMessage(PLAYER_DATA_FIND_ERROR);
                return;
            } else if (result == FindResult.Result.FOUND_MULTIPLE) {
                sender.sendMessage(String.format(MULTIPLE_FOUND, targetPlayerName));
                return;
            }

            TData playerData = findResult.getFirstDataFound();
            if (playerData == null) {
                return;
            }
            MuteEntry currentMute = playerData.getFirstValidMuteEntry();
            if (currentMute == null) {
                sender.sendMessage(String.format(NOT_MUTED, playerData.getName()));
                return;
            }

            currentMute.unmute(unmutingUniqueId);

            if (!plugin.getDataHandler().updateData(playerData, DataField.of(DataField.DataFieldOption.MUTES), false)) {
                sender.sendMessage(PLAYER_DATA_UPDATE_ERROR);
                return;
            }

            sender.sendMessage(String.format(PLAYER_UNMUTED, playerData.getGroup().getColor() + playerData.getName()));
        }
    }
}