package br.com.midup.common.networking.packet.in;

import br.com.midup.api.player.Group;
import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.networking.packet.PacketHandler;
import br.com.midup.common.networking.packet.PacketIn;
import br.com.midup.common.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataInput;

import java.util.LinkedHashMap;
import java.util.Map;

public class PacketInSetPermissions extends PacketIn {

    private Group group;
    private Role role;

    private Map<String, Boolean> permissions;

    public PacketInSetPermissions() {
        super(PacketType.In.SET_PERMISSIONS);
        this.permissions = new LinkedHashMap<>();
    }

    public Group getGroup() {
        return group;
    }

    public Role getRole() {
        return role;
    }

    public int getAmount() {
        return permissions.size();
    }

    public Map<String, Boolean> getPermissions() {
        return permissions;
    }

    @Override
    public void readPacketData(ByteArrayDataInput input) {
        int groupId = input.readInt();
        this.group = Group.getGroup(groupId);
        if (group == null) {
            throw new IllegalArgumentException("Invalid group id: " + groupId);
        }

        int roleId = input.readInt();
        this.role = MidupCommon.getInstance().getPlayerList().getSpecificRole(roleId);
        if (role == null) {
            throw new IllegalArgumentException("Invalid role id: " + roleId);
        }

        int positivePermissions = input.readInt();
        for (int i = 0; i < positivePermissions; i++) {
            this.permissions.put(input.readUTF(), true);
        }

        int negativePermissions = input.readInt();
        for (int i = 0; i < negativePermissions; i++) {
            this.permissions.put(input.readUTF(), false);
        }
    }

    @Override
    public void processPacketData(PacketHandler<?, ?, ?> handler) {
        handler.processSetPermissions(this);
    }
}