package br.com.midup.common.networking.packet.in;

import br.com.midup.api.player.Group;
import br.com.midup.common.networking.packet.PacketHandler;
import br.com.midup.common.networking.packet.PacketIn;
import br.com.midup.common.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataInput;

import java.util.UUID;

public class PacketInGroupRequest extends PacketIn {

    private UUID uniqueId;
    private Group group;
    private int duration;

    public PacketInGroupRequest() {
        super(PacketType.In.GROUP_REQUEST);
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public Group getGroup() {
        return group;
    }

    public int getDuration() {
        return duration;
    }

    public boolean isPermanent() {
        return duration == -1;
    }

    @Override
    public void readPacketData(ByteArrayDataInput input) {
        long most = input.readLong();
        long least = input.readLong();
        this.uniqueId = new UUID(most, least);

        int groupId = input.readInt();
        this.group = Group.getGroup(groupId);
        this.duration = input.readInt();
    }

    @Override
    public void processPacketData(PacketHandler<?, ?, ?> handler) {
        handler.processGroupRequest(this);
    }
}