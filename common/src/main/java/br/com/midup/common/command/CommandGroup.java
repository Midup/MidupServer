package br.com.midup.common.command;

import static br.com.midup.api.data.DataField.DataFieldOption.*;
import static br.com.midup.util.Messages.*;

import br.com.midup.api.command.MidupCommand;
import br.com.midup.api.player.Group;
import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.api.data.DataField;
import br.com.midup.api.data.FindResult;
import br.com.midup.common.networking.packet.out.PacketOutGroupRequest;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.util.ColorUtil;
import br.com.midup.util.Messages;
import br.com.midup.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.*;

public class CommandGroup<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role>
        extends MidupCommand implements TabCompleter {

    private static final String CANNOT_SET_OWN_GROUP = ColorUtil.RED + "You cannot set your own group!";

    private static final String SAME_GROUP = ColorUtil.RED + "The specified player already is in the specified group!";

    private static final String GROUP_NOT_FOUND = ColorUtil.RED + "The group '%s' was not found!";

    private static final String PLAYER_GROUP_SET = "%s" + ColorUtil.GRAY + " is now a %s" + ColorUtil.GRAY + "!";

    private static final String NEW_GROUP_TOO_HIGH = ColorUtil.RED + "You don't have permission to set the group %s "
            + ColorUtil.RED + "to someone!";

    private static final String TARGET_GROUP_TOO_HIGH = ColorUtil.RED
            + "You don't have permission to modify the group of a %s" + ColorUtil.RED + "!";

    private static final String AVAILABLE_GROUPS = ColorUtil.YELLOW + "Available groups: " + ColorUtil.RESET + "%s";

    private final MidupCommon<TPlayer, TData, TRole> plugin;

    public CommandGroup(MidupCommon<TPlayer, TData, TRole> plugin) {
        super("group", null, "midup.command.group", "/%s <player/uuid> <group> [expiry]");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String label, String[] args) {
        if (args.length < 2) {
            sender.sendMessage(String.format(getSyntax(), name));
            return;
        }

        Group senderGroup = null;
        if (sender instanceof Player) {
            senderGroup = plugin.getPlayerList().getPlayer(((Player) sender)).getData().getGroup();
        }
        Group newGroup = Group.getGroup(args[1]);
        if (newGroup == null) {
            sender.sendMessage(String.format(GROUP_NOT_FOUND, args[1]));
            Set<Group> available = getAvailableGroups(sender);
            sender.sendMessage(String.format(AVAILABLE_GROUPS,
                    available.size() < 1 ? NONE : Util.coloredStringJoin(available, JOINT)));
            return;
        }

        int duration = -1;
        if (args.length > 2) {
            duration = Util.parseTimeString(args[2]);
            if (duration == -1) {
                sender.sendMessage(INVALID_TIME_STRING);
                return;
            }
        }

        UUID uniqueId = Util.uniqueId(args[0]);
        Player player;

        if (uniqueId == null) {
            player = Bukkit.getPlayer(args[0]);
        } else {
            player = Bukkit.getPlayer(uniqueId);
        }

        if (player == null) {
            sender.sendMessage(String
                    .format(uniqueId == null ? PLAYER_NOT_FOUND_SEARCHING_DB : UUID_NOT_FOUND_SEARCHING_DB, args[0]));

            Runnable groupTask = new AsyncGroupSetTask(sender, senderGroup, args[0], newGroup, duration);
            plugin.getServer().getScheduler().runTaskAsynchronously(plugin, groupTask);
            return;
        } else if (player.equals(sender)) {
            sender.sendMessage(CANNOT_SET_OWN_GROUP);
            return;
        }

        TPlayer tPlayer = plugin.getPlayerList().getPlayer(player);
        TData tData = tPlayer.getData();
        Group targetGroup = tData.getGroup();
        if (!this.checkSetGroupPermission(sender, senderGroup, newGroup, targetGroup)) {
            Set<Group> available = this.getAvailableGroups(sender);
            sender.sendMessage(String.format(AVAILABLE_GROUPS,
                    available.size() < 1 ? NONE : Util.coloredStringJoin(available, JOINT)));
            return;
        }

        sender.sendMessage(String.format(PLAYER_GROUP_SET, player.getDisplayName(), newGroup.getColoredName()));
        player.sendMessage(String.format(YOUR_GROUP_SET, newGroup.getColoredName()));

        tData.setGroup(newGroup);
        tData.setGroupExpiry(duration == -1 ? -1 : Util.unixTimestamp() + duration);
        player.setDisplayName(newGroup.getColor() + player.getName() + ColorUtil.RESET);
        plugin.getPermissions().updatePlayer(tPlayer, true, duration != -1, false);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        Group playerGroup = null;
        if (sender instanceof Player) {
            playerGroup = plugin.getPlayerList().getPlayer(((Player) sender)).getData().getGroup();
        }
        List<String> matches = new ArrayList<>();
        if (args.length == 1) {
            String match = args[0].toLowerCase();
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getName().toLowerCase().startsWith(match)) {
                    matches.add(player.getName());
                }
            }
            return matches;
        } else if (args.length == 2) {
            String match = args[1].toLowerCase();
            for (Group group : Group.values()) {
                if ((playerGroup == null || playerGroup.canSetGroup(group))
                        && group.getName().toLowerCase().startsWith(match)) {
                    matches.add(group.getName());
                }
            }
        }
        return matches;
    }

    private boolean checkSetGroupPermission(CommandSender sender, Group senderGroup, Group newGroup,
                                            Group targetGroup) {
        if (senderGroup == null) {
            return true;
        }

        if (!senderGroup.canSetGroup(newGroup)) {
            sender.sendMessage(String.format(NEW_GROUP_TOO_HIGH, newGroup.getColoredName()));
            return false;
        }

        if (!senderGroup.canSetGroup(targetGroup)) {
            sender.sendMessage(String.format(TARGET_GROUP_TOO_HIGH, targetGroup.getColoredName()));
            return false;
        }

        return true;
    }

    private Set<Group> getAvailableGroups(CommandSender sender) {
        Set<Group> groups = new LinkedHashSet<>();
        Group playerGroup = null;
        if (sender instanceof Player) {
            playerGroup = plugin.getPlayerList().getPlayer(((Player) sender)).getData().getGroup();
        }
        for (Group group : Group.values()) {
            if (playerGroup == null || playerGroup.canSetGroup(group)) {
                groups.add(group);
            }
        }
        return groups;
    }

    private class AsyncGroupSetTask implements Runnable {

        private final CommandSender sender;
        private final Group senderGroup;
        private final String targetPlayerName;
        private final Group newGroup;
        private final int duration;

        private AsyncGroupSetTask(CommandSender sender, Group senderGroup, String targetPlayerName, Group newGroup,
                                  int duration) {
            this.sender = sender;
            this.senderGroup = senderGroup;
            this.targetPlayerName = targetPlayerName;
            this.newGroup = newGroup;
            this.duration = duration;
        }

        @Override
        public void run() {
            UUID uniqueId = Util.uniqueId(targetPlayerName);
            FindResult<TPlayer, TData, TRole> findResult;

            if (uniqueId == null) {
                findResult = plugin.getDataHandler().findDataByName(targetPlayerName,
                        DataField.of(NAME, GROUP, GROUP_EXPIRY), false);
            } else {
                findResult = plugin.getDataHandler().findDataByUniqueId(uniqueId,
                        DataField.of(NAME, GROUP, GROUP_EXPIRY), false);
            }

            FindResult.Result result = findResult.getResult();
            if (result == FindResult.Result.NOT_FOUND) {
                sender.sendMessage(
                        String.format(uniqueId == null ? PLAYER_NOT_IN_DB : UUID_NOT_IN_DB, targetPlayerName));
                return;
            } else if (result == FindResult.Result.ERROR) {
                sender.sendMessage(PLAYER_DATA_FIND_ERROR);
                return;
            } else if (result == FindResult.Result.FOUND_MULTIPLE) {
                sender.sendMessage(String.format(MULTIPLE_FOUND, targetPlayerName));
                return;
            }

            TData playerData = findResult.getFirstDataFound();
            if (playerData == null) {
                return;
            }

            Group oldGroup = playerData.getGroup();
            if (oldGroup == newGroup) {
                sender.sendMessage(SAME_GROUP);
                return;
            }
            if (!checkSetGroupPermission(sender, senderGroup, newGroup, oldGroup)) {
                Set<Group> available = getAvailableGroups(sender);
                sender.sendMessage(String.format(AVAILABLE_GROUPS,
                        available.size() < 1 ? Messages.NONE : Util.coloredStringJoin(available, JOINT)));
                return;
            }

            playerData.setGroup(newGroup);
            playerData.setGroupExpiry(duration);

            if (!plugin.getDataHandler().updateData(playerData, DataField.of(GROUP, GROUP_EXPIRY), false)) {
                sender.sendMessage(Messages.PLAYER_DATA_UPDATE_ERROR);
                return;
            }
            plugin.getProxyConnection()
                    .sendPacket(new PacketOutGroupRequest(playerData.getUniqueId(), newGroup, duration));

            sender.sendMessage(String.format(PLAYER_GROUP_SET, oldGroup.getColor() + playerData.getName(),
                    newGroup.getColoredName()));
        }
    }
}
