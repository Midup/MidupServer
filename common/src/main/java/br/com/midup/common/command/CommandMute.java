package br.com.midup.common.command;

import static br.com.midup.util.Messages.*;

import br.com.midup.api.command.MidupCommand;
import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.api.punishment.MuteEntry;
import br.com.midup.util.ColorUtil;
import br.com.midup.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.UUID;

public class CommandMute<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role>
        extends MidupCommand {

    private static final String PLAYER_MUTED = "%s" + ColorUtil.RED + " has been muted!";

    private static final String YOU_HAVE_BEEN_MUTED = ColorUtil.BOLD_C + "You've been muted!" + ColorUtil.RED
            + "\nMuted by: %s" + ColorUtil.RED + "\nReason: " + ColorUtil.RESET + "%s";

    private MidupCommon<TPlayer, TData, TRole> plugin;

    public CommandMute(MidupCommon<TPlayer, TData, TRole> plugin) {
        super("mute", null, "midup.command.mute", "/%s <player/uuid> <reason...>");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String label, String[] args) {
        if (args.length < 2) {
            sender.sendMessage(String.format(getSyntax(), name));
            return;
        }

        String[] reasonArray = Arrays.copyOfRange(args, 1, args.length);
        String reason = Util.stringJoin(reasonArray, " ");

        UUID uniqueId = Util.uniqueId(args[0]);
        Player player;

        if (uniqueId == null) {
            player = Bukkit.getPlayer(args[0]);
        } else {
            player = Bukkit.getPlayer(uniqueId);
        }

        if (player == null) {
            sender.sendMessage(String.format(uniqueId == null ? PLAYER_NOT_FOUND : UUID_NOT_FOUND, args[0]));
            return;
        }

        if (player.equals(sender)) {
            sender.sendMessage(CANNOT_SELF_MUTE);
            return;
        }

        TPlayer tPlayer = plugin.getPlayerList().getPlayer(player);
        TData tData = tPlayer.getData();
        if (tData.getFirstValidMuteEntry() != null) {
            sender.sendMessage(String.format(ALREADY_MUTED, player.getName()));
            return;
        }

        if (tData.getGroup().isStaff()) {
            sender.sendMessage(CANNOT_MUTE_STAFF);
            return;
        }

        UUID mutingUniqueId = sender instanceof Player ? ((Player) sender).getUniqueId() : Util.CONSOLE_UUID;
        tData.getMutes().add(new MuteEntry(mutingUniqueId, Util.unixTimestamp(), -1, reason));

        sender.sendMessage(String.format(PLAYER_MUTED, player.getDisplayName()));
        player.sendMessage(String.format(YOU_HAVE_BEEN_MUTED, Util.getDisplayName(sender), reason));
        plugin.getPermissions().updatePlayer(tPlayer, false, false, true);
    }

}