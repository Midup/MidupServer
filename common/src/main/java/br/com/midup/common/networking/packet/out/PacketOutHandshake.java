package br.com.midup.common.networking.packet.out;

import br.com.midup.common.networking.ServerType;
import br.com.midup.common.networking.packet.PacketOut;
import br.com.midup.common.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataOutput;
import org.bukkit.Bukkit;

public class PacketOutHandshake extends PacketOut {

    private final ServerType serverType;

    public PacketOutHandshake(ServerType serverType) {
        super(PacketType.Out.HANDSHAKE);
        this.serverType = serverType;
    }

    @Override
    public void writePacketData(ByteArrayDataOutput output) {
        output.writeInt(serverType.getId());
        output.writeInt(Bukkit.getPort());
    }
}