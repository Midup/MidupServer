package br.com.midup.common.player;

import br.com.midup.api.player.Group;
import br.com.midup.api.player.Role;
import br.com.midup.common.data.BasePlayerData;
import org.bukkit.entity.Player;

import java.util.*;

public abstract class BasePlayerList<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role> {

    protected final Object playerLock;
    protected final Map<UUID, TPlayer> players;

    public BasePlayerList() {
        this.playerLock = new Object();
        this.players = new HashMap<>();
    }

    public final void registerPlayer(Player player, TData playerData) {
        synchronized (playerLock) {
            players.put(player.getUniqueId(), createPlayer(player, playerData));
        }
    }

    public final TPlayer getPlayer(Player player) {
        synchronized (playerLock) {
            return getPlayer(player.getUniqueId());
        }
    }

    public final TPlayer getPlayer(UUID uniqueId) {
        synchronized (playerLock) {
            return players.get(uniqueId);
        }
    }

    public final Set<TPlayer> getPlayersByGroup(Group... groups) {
        synchronized (playerLock) {
            return getPlayersByGroup(Arrays.asList(groups));
        }
    }

    public final Set<TPlayer> getPlayersByGroup(Collection<Group> groups) {
        synchronized (playerLock) {
            Set<TPlayer> targetPlayers = new HashSet<TPlayer>();
            for (TPlayer tPlayer : players.values()) {
                if (groups.contains(tPlayer.getData().getGroup())) {
                    targetPlayers.add(tPlayer);
                }
            }
            return targetPlayers;
        }
    }

    public final Set<TPlayer> getPlayersByGroupRole(Group group, Role role) {
        synchronized (playerLock) {
            return getPlayersByGroupRole(Collections.singletonList(group), Collections.singletonList(role));
        }
    }

    public final Set<TPlayer> getPlayersByGroupRole(Collection<Group> groups, Collection<Role> roles) {
        synchronized (playerLock) {
            Set<TPlayer> targetPlayers = new HashSet<TPlayer>();
            for (TPlayer tPlayer : players.values()) {
                if (groups.contains(tPlayer.getData().getGroup()) && roles.contains(tPlayer.getRole())) {
                    targetPlayers.add(tPlayer);
                }
            }
            return targetPlayers;
        }
    }

    public final Set<TPlayer> getPlayersByRole(Role... role) {
        synchronized (playerLock) {
            return getPlayersByRole(Arrays.asList(role));
        }
    }

    public final Set<TPlayer> getPlayersByRole(Collection<Role> roles) {
        synchronized (playerLock) {
            Set<TPlayer> targetPlayers = new HashSet<TPlayer>();
            for (TPlayer tPlayer : players.values()) {
                if (roles.contains(tPlayer.getRole())) {
                    targetPlayers.add(tPlayer);
                }
            }
            return targetPlayers;
        }
    }

    public final void unregisterPlayer(Player player) {
        synchronized (playerLock) {
            unregisterPlayer(player.getUniqueId());
        }
    }

    public final void unregisterPlayer(UUID uniqueId) {
        synchronized (playerLock) {
            players.remove(uniqueId);
        }
    }

    public final Collection<TPlayer> getPlayers() {
        synchronized (playerLock) {
            return Collections.unmodifiableCollection(players.values());
        }
    }

    public abstract TRole getSpecificRole(int id);

    public abstract Set<TRole> getSpecificRoles();

    protected abstract TPlayer createPlayer(Player player, TData playerData);
}