package br.com.midup.common.networking;

public enum ConnectionState {

    DISCONNECTED,
    CONNECTING,
    AWAITING_HANDSHAKE,
    CONNECTED;
}