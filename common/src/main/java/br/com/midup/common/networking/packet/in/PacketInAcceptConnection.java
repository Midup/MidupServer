package br.com.midup.common.networking.packet.in;

import br.com.midup.common.networking.packet.PacketHandler;
import br.com.midup.common.networking.packet.PacketIn;
import br.com.midup.common.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataInput;

public class PacketInAcceptConnection extends PacketIn {

    private String serverName;

    public PacketInAcceptConnection() {
        super(PacketType.In.ACCEPT_CONNECTION);
    }

    public String getServerName() {
        return serverName;
    }

    @Override
    public void readPacketData(ByteArrayDataInput input) {
        this.serverName = input.readUTF();
    }

    @Override
    public void processPacketData(PacketHandler<?, ?, ?> handler) {
        handler.processAcceptConnection(this);
    }
}