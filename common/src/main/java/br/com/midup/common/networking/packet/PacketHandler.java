package br.com.midup.common.networking.packet;

import static br.com.midup.util.Messages.YOUR_GROUP_SET;
import static br.com.midup.util.Messages.YOU_HAVE_BEEN_BANNED;
import static br.com.midup.util.Messages.YOU_HAVE_BEEN_TEMP_BANNED;

import br.com.midup.api.player.Group;
import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.common.logging.FormattedLogger;
import br.com.midup.common.networking.ConnectionState;
import br.com.midup.common.networking.ProxyConnection;
import br.com.midup.common.networking.packet.in.*;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.util.ColorUtil;
import br.com.midup.util.Util;
import org.bukkit.entity.Player;

public class PacketHandler<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role> {

    private final MidupCommon<TPlayer, TData, TRole> plugin;
    private final ProxyConnection<TPlayer, TData, TRole> connection;
    private final FormattedLogger logger;

    public PacketHandler(MidupCommon<TPlayer, TData, TRole> plugin,
                         ProxyConnection<TPlayer, TData, TRole> connection) {
        this.plugin = plugin;
        this.connection = connection;
        this.logger = connection.getLogger();
    }

    public void processAcceptConnection(PacketInAcceptConnection packet) {
        logger.log("The server has been accepted by the proxy server!");
        logger.log("This server is now identified as '%s'", packet.getServerName());
        connection.updateConnectionState(ConnectionState.CONNECTED);
    }

    public void processDisconnect(PacketInDisconnect packet) {
        logger.log("Received disconnect request from the proxy!");
        connection.updateConnectionState(ConnectionState.DISCONNECTED);
        logger.log("Disconnected! Reason: %s", packet.getReason());
        connection.disconnect(packet.getReason());
    }

    public void processSetPermission(PacketInSetPermission packet) {
        logger.log("Received a set permission request for [%s][%s]", packet.getGroup(), packet.getRole());
        plugin.getPermissions().setPermission(packet.getGroup(), packet.getRole(), packet.getPermission(),
                packet.getValue());
    }

    public void processUnsetPermission(PacketInUnsetPermission packet) {
        logger.log("Received a unset permission request for [%s][%s]", packet.getGroup(), packet.getRole());
        plugin.getPermissions().unsetPermission(packet.getGroup(), packet.getRole(), packet.getPermission());
    }

    public void processSetPermissions(PacketInSetPermissions packet) {
        logger.log("Received %d set permission requests for [%s][%s]", packet.getAmount(), packet.getGroup(),
                packet.getRole());
        plugin.getPermissions().setPermissions(packet.getGroup(), packet.getRole(), packet.getPermissions());
    }

    public void processUnsetPermissions(PacketInUnsetPermissions packet) {
        logger.log("Received %d unset permission requests for [%s][%s]", packet.getAmount(), packet.getGroup(),
                packet.getRole());
        plugin.getPermissions().unsetPermissions(packet.getGroup(), packet.getRole(), packet.getPermissions());
    }

    public void processGroupRequest(PacketInGroupRequest packet) {
        TPlayer tPlayer = plugin.getPlayerList().getPlayer(packet.getUniqueId());
        if (tPlayer == null) {
            return;
        }
        Player player = tPlayer.getPlayer();
        TData tData = tPlayer.getData();
        Group newGroup = packet.getGroup();
        int duration = packet.getDuration();

        player.sendMessage(String.format(YOUR_GROUP_SET, newGroup.getColoredName()));

        tData.setGroup(newGroup);
        tData.setGroupExpiry(duration == -1 ? -1 : Util.unixTimestamp() + duration);

        player.setDisplayName(newGroup.getColor() + player.getName() + ColorUtil.RESET);
        plugin.getPermissions().updatePlayer(tPlayer, true, duration != -1, false);
    }

    public void processBanRequest(PacketInBanRequest packet) {
        TPlayer tPlayer = plugin.getPlayerList().getPlayer(packet.getUniqueId());
        if (tPlayer == null) {
            return;
        }
        Player player = tPlayer.getPlayer();

        if (packet.isPermanent()) {
            player.kickPlayer(String.format(YOU_HAVE_BEEN_BANNED, packet.getBanningName(), packet.getReason()));
        } else {
            player.kickPlayer(String.format(YOU_HAVE_BEEN_TEMP_BANNED, packet.getBanningName(), packet.getReason(),
                    Util.getLongTimeString(packet.getDuration())));
        }
    }
}