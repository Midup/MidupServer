package br.com.midup.common.player;

import br.com.midup.api.player.Role;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.api.punishment.MuteEntry;
import br.com.midup.util.ColoredName;
import org.bukkit.entity.Player;

public abstract class BasePlayer<TData extends BasePlayerData, TRole extends Role> implements ColoredName {

    protected final Player player;
    protected final TData playerData;
    protected TRole role;
    protected MuteEntry currentMute;

    protected BasePlayer(Player player, TData playerData) {
        this.player = player;
        this.playerData = playerData;
        this.currentMute = null;
    }

    public final Player getPlayer() {
        return player;
    }

    public final TData getData() {
        return playerData;
    }

    public final TRole getRole() {
        return role;
    }

    public final void setRole(TRole role) {
        this.role = role;
    }

    @Override
    public final String getColoredName() {
        return this.player.getDisplayName();
    }

    public synchronized final MuteEntry getCurrentMute() {
        return currentMute;
    }

    public synchronized final void setCurrentMute(MuteEntry currentMute) {
        this.currentMute = currentMute;
    }

}