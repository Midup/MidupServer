package br.com.midup.common.command;

import static br.com.midup.util.Messages.*;

import br.com.midup.api.command.MidupCommand;
import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.api.punishment.MuteEntry;
import br.com.midup.util.ColorUtil;
import br.com.midup.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.UUID;

public class CommandTempmute<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role>
        extends MidupCommand {

    private static final String PLAYER_TEMP_MUTED = "%s" + ColorUtil.RED + " has been muted temporarily!";

    private static final String YOU_HAVE_BEEN_TEMP_MUTED = ColorUtil.BOLD_C + "You've been temporarily muted!"
            + ColorUtil.RED + "\nMuted by: %s" + ColorUtil.RED + "\nReason: " + ColorUtil.RESET + "%s" + ColorUtil.RED
            + "\nExpires in: " + ColorUtil.RESET + "%s";

    private MidupCommon<TPlayer, TData, TRole> plugin;

    public CommandTempmute(MidupCommon<TPlayer, TData, TRole> plugin) {
        super("tempmute", null, "midup.command.tempmute", "/%s <player/uuid> <duration> <reason...>");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String label, String[] args) {
        if (args.length < 3) {
            sender.sendMessage(String.format(getSyntax(), name));
            return;
        }

        String[] reasonArray = Arrays.copyOfRange(args, 2, args.length);
        String reason = Util.stringJoin(reasonArray, " ");
        int duration = Util.parseTimeString(args[1]);

        if (duration == -1) {
            sender.sendMessage(String.format(INVALID_TIME_STRING, args[1]));
            return;
        }

        UUID uniqueId = Util.uniqueId(args[0]);
        Player player;

        if (uniqueId == null) {
            player = Bukkit.getPlayer(args[0]);
        } else {
            player = Bukkit.getPlayer(uniqueId);
        }

        if (player == null) {
            sender.sendMessage(String.format(uniqueId == null ? PLAYER_NOT_FOUND : UUID_NOT_FOUND, args[0]));
            return;
        } else if (player.equals(sender)) {
            sender.sendMessage(CANNOT_SELF_MUTE);
            return;
        }

        TPlayer tPlayer = plugin.getPlayerList().getPlayer(player);
        TData tData = tPlayer.getData();
        if (tData.getFirstValidMuteEntry() != null) {
            sender.sendMessage(String.format(ALREADY_MUTED, player.getName()));
            return;
        }

        if (tData.getGroup().isStaff()) {
            sender.sendMessage(CANNOT_MUTE_STAFF);
            return;
        }

        UUID mutingUniqueId = sender instanceof Player ? ((Player) sender).getUniqueId() : Util.CONSOLE_UUID;
        int now = Util.unixTimestamp();
        tData.getMutes().add(new MuteEntry(mutingUniqueId, now, now + duration, reason));
        sender.sendMessage(String.format(PLAYER_TEMP_MUTED, player.getDisplayName()));
        player.sendMessage(String.format(YOU_HAVE_BEEN_TEMP_MUTED, Util.getDisplayName(sender), reason,
                Util.getLongTimeString(duration)));
        plugin.getPermissions().updatePlayer(tPlayer, false, false, true);
    }

}