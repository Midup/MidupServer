package br.com.midup.common.networking.packet.in;

import br.com.midup.common.networking.packet.PacketHandler;
import br.com.midup.common.networking.packet.PacketIn;
import br.com.midup.common.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataInput;

import java.util.UUID;

public class PacketInBanRequest extends PacketIn {

    private UUID uniqueId;
    private UUID banningUniqueId;
    private String banningName;
    private String reason;
    private int duration;

    public PacketInBanRequest() {
        super(PacketType.In.BAN_REQUEST);
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public UUID getBanningUniqueId() {
        return banningUniqueId;
    }

    public String getBanningName() {
        return banningName;
    }

    public String getReason() {
        return reason;
    }

    public int getDuration() {
        return duration;
    }

    public boolean isPermanent() {
        return duration == -1;
    }

    @Override
    public void readPacketData(ByteArrayDataInput input) {
        long mostUniqueId = input.readLong();
        long leastUniqueId = input.readLong();
        this.uniqueId = new UUID(mostUniqueId, leastUniqueId);

        long mostBanningUniqueId = input.readLong();
        long leastBanningUniqueId = input.readLong();
        this.banningUniqueId = new UUID(mostBanningUniqueId, leastBanningUniqueId);

        this.banningName = input.readUTF();
        this.reason = input.readUTF();
        this.duration = input.readInt();
    }

    @Override
    public void processPacketData(PacketHandler<?, ?, ?> handler) {
        handler.processBanRequest(this);
    }
}

