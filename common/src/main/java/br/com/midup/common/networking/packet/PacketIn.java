package br.com.midup.common.networking.packet;

import br.com.midup.common.networking.packet.PacketType.PacketTypeIn;
import com.google.common.io.ByteArrayDataInput;

public abstract class PacketIn extends Packet {

    public PacketIn(PacketTypeIn packetType) {
        super(packetType);
    }

    @Override
    public PacketTypeIn getPacketType() {
        return (PacketTypeIn) this.packetType;
    }

    public abstract void readPacketData(ByteArrayDataInput input);

    public abstract void processPacketData(PacketHandler<?, ?, ?> handler);
}