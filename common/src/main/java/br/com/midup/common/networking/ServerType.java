package br.com.midup.common.networking;

import java.util.HashMap;
import java.util.Map;

public enum ServerType {

    HUB(0),
    BEDWARS(1);

    private static final Map<Integer, ServerType> BY_ID = new HashMap<>();
    private static final Map<String, ServerType> BY_ALIAS = new HashMap<>();

    private final int id;

    static {
        for (ServerType serverType : values()) {
            BY_ID.put(serverType.id, serverType);
            BY_ALIAS.put(serverType.name().toLowerCase(), serverType);
        }
    }

    private ServerType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static ServerType getServerType(int id) {
        return BY_ID.get(id);
    }

    public static ServerType getServerType(String alias) {
        return BY_ALIAS.get(alias.toLowerCase());
    }
}