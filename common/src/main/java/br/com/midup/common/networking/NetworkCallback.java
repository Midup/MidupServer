package br.com.midup.common.networking;

import br.com.midup.common.networking.packet.PacketIn;

public interface NetworkCallback {

    public void onReceivePacket(PacketIn packetIn);
}