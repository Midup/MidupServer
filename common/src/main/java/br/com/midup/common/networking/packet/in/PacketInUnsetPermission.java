package br.com.midup.common.networking.packet.in;

import br.com.midup.api.player.Group;
import br.com.midup.api.player.Role;
import br.com.midup.common.MidupCommon;
import br.com.midup.common.networking.packet.PacketHandler;
import br.com.midup.common.networking.packet.PacketIn;
import br.com.midup.common.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataInput;

public class PacketInUnsetPermission extends PacketIn {

    private Group group;
    private Role role;

    private String permission;

    public PacketInUnsetPermission() {
        super(PacketType.In.UNSET_PERMISSION);
    }

    public Group getGroup() {
        return group;
    }

    public Role getRole() {
        return role;
    }

    public String getPermission() {
        return permission;
    }

    @Override
    public void readPacketData(ByteArrayDataInput input) {
        int groupId = input.readInt();
        this.group = Group.getGroup(groupId);
        if (group == null) {
            throw new IllegalArgumentException("Invalid group id: " + groupId);
        }

        int roleId = input.readInt();
        this.role = MidupCommon.getInstance().getPlayerList().getSpecificRole(roleId);
        if (role == null) {
            throw new IllegalArgumentException("Invalid role id: " + roleId);
        }

        this.permission = input.readUTF();
    }

    @Override
    public void processPacketData(PacketHandler<?, ?, ?> handler) {
        handler.processUnsetPermission(this);
    }
}