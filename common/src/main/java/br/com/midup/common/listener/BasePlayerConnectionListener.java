package br.com.midup.common.listener;

import static br.com.midup.api.data.DataField.DataFieldOption.GROUP;
import static br.com.midup.api.data.DataField.DataFieldOption.NAME;
import static br.com.midup.util.Messages.UNKNOWN;

import java.util.UUID;

import br.com.midup.api.player.Role;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import br.com.midup.common.MidupCommon;
import br.com.midup.api.punishment.BanEntry;
import br.com.midup.common.data.BasePlayerData;
import br.com.midup.api.data.DataField;
import br.com.midup.api.data.FindResult;
import br.com.midup.common.networking.ConnectionState;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.util.ColorUtil;
import br.com.midup.util.Util;

public abstract class BasePlayerConnectionListener<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role>
        implements Listener {

    private static final String NO_PROXY_CONNECTION = ColorUtil.DARK_RED + "[Error 100] " + ColorUtil.RED
            + "There's no connection with the proxy server. You cannot login!";

    private static final String DATA_FIND_ERROR = ColorUtil.DARK_RED + "[Error 101] " + ColorUtil.RED
            + "An error occurred while loading your player data from the database!";

    private static final String DATA_INSERT_ERROR = ColorUtil.DARK_RED + "[Error 102] " + ColorUtil.RED
            + "An error occurred while creating new player data for your account!";

    private static final String DATA_RETRIEVE_FAIL = ColorUtil.DARK_RED + "[Error 103] " + ColorUtil.RED
            + "An error occurred while trying to retrieve your cached player data!";

    private static final String YOU_ARE_BANNED = ColorUtil.BOLD_C + "You're banned!" + ColorUtil.RED + "\nBanned by: %s"
            + ColorUtil.RED + "\nWhen: " + ColorUtil.RESET + "%s" + ColorUtil.RED + "\nReason: " + ColorUtil.RESET
            + "%s";

    private static final String YOU_ARE_TEMP_BANNED = ColorUtil.BOLD_C + "You're temporarily banned!" + ColorUtil.RED
            + "\nBanned by: %s" + ColorUtil.RED + "\nWhen: " + ColorUtil.RESET + "%s" + ColorUtil.RED + "\nReason: "
            + ColorUtil.RESET + "%s" + ColorUtil.RED + "\nExpires in: " + ColorUtil.RESET + "%s";

    protected MidupCommon<TPlayer, TData, TRole> plugin;

    public BasePlayerConnectionListener(MidupCommon<TPlayer, TData, TRole> plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public final void onBasePreLogin(AsyncPlayerPreLoginEvent event) {
        if (plugin.getProxyConnection().getConnectionState() != ConnectionState.CONNECTED) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, NO_PROXY_CONNECTION);
            return;
        }

        UUID uniqueId = event.getUniqueId();
        TData playerData;
        plugin.getDataHandler().retrievePlayerData(uniqueId);

        FindResult<TPlayer, TData, TRole> findResult = plugin.getDataHandler().findDataByUniqueId(uniqueId);
        FindResult.Result result = findResult.getResult();
        if (result == FindResult.Result.NOT_FOUND) {
            playerData = plugin.getDataHandler().initializePlayerData(event.getUniqueId(), event.getName());
            if (!plugin.getDataHandler().updateData(playerData)) {
                event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, DATA_INSERT_ERROR);
                return;
            }
        } else if (result == FindResult.Result.ERROR) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, DATA_FIND_ERROR);
            return;
        } else {
            playerData = findResult.getFirstDataFound();
        }

        BanEntry entry = playerData.getFirstValidBanEntry();

        if (entry != null) {
            String banningName = UNKNOWN;
            String when = Util.getLongTimeString(Util.unixTimestamp() - entry.getBanDate()) + " ago";
            if (entry.getBanningUniqueId().equals(Util.CONSOLE_UUID)) {
                banningName = Util.CONSOLE_NAME;
            } else {
                findResult = plugin.getDataHandler().findDataByUniqueId(entry.getBanningUniqueId(),
                        DataField.of(NAME, GROUP), false);
                result = findResult.getResult();
                if (result == FindResult.Result.FOUND_ONE || result == FindResult.Result.FOUND_MULTIPLE) {
                    TData banningPlayer = findResult.getFirstDataFound();
                    banningName = banningPlayer.getGroup().getColor() + banningPlayer.getName();
                }
            }
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED,
                    entry.isPermanent() ? String.format(YOU_ARE_BANNED, banningName, when, entry.getReason())
                            : String.format(YOU_ARE_TEMP_BANNED, banningName, when, entry.getReason(),
                            Util.getLongTimeString(entry.getExpiry() - Util.unixTimestamp())));
            return;
        }

        plugin.getDataHandler().cachePlayerData(uniqueId, playerData);

    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public final void onBaseLogin(PlayerLoginEvent event) {
        Player player = event.getPlayer();
        TData tData = plugin.getDataHandler().retrievePlayerData(player.getUniqueId());

        if (tData == null) {
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, DATA_RETRIEVE_FAIL);
            return;
        }

        plugin.getPlayerList().registerPlayer(player, tData);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public final void onBaseJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        TPlayer tPlayer = plugin.getPlayerList().getPlayer(player);
        player.setDisplayName(tPlayer.getData().getGroup().getColor() + player.getName() + ChatColor.RESET);
        plugin.getPermissions().registerPlayer(tPlayer);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public final void onBaseQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        TPlayer tPlayer = plugin.getPlayerList().getPlayer(player);
        plugin.getDataHandler().updateData(tPlayer.getData(), DataField.all(), true);
        plugin.getPlayerList().unregisterPlayer(event.getPlayer());
        plugin.getPermissions().unregisterPlayer(tPlayer);
    }
}