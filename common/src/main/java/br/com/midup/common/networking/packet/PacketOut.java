package br.com.midup.common.networking.packet;

import br.com.midup.common.networking.packet.PacketType.PacketTypeOut;
import com.google.common.io.ByteArrayDataOutput;

public abstract class PacketOut extends Packet {

    public PacketOut(PacketTypeOut packetType) {
        super(packetType);
    }

    @Override
    public PacketTypeOut getPacketType() {
        return (PacketTypeOut) this.packetType;
    }

    public abstract void writePacketData(ByteArrayDataOutput output);
}