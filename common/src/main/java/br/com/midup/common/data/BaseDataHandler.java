package br.com.midup.common.data;

import static br.com.midup.api.data.DataField.DataFieldOption.BANS;
import static br.com.midup.api.data.DataField.DataFieldOption.GROUP;
import static br.com.midup.api.data.DataField.DataFieldOption.GROUP_EXPIRY;
import static br.com.midup.api.data.DataField.DataFieldOption.MUTES;
import static br.com.midup.api.data.DataField.DataFieldOption.NAME;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import br.com.midup.api.data.DataField;
import br.com.midup.api.data.FindResult;
import br.com.midup.api.punishment.BanEntry;
import br.com.midup.api.punishment.MuteEntry;
import br.com.midup.common.networking.ServerType;
import org.bson.Document;
import org.bson.UuidRepresentation;
import org.bson.codecs.UuidCodec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;

import br.com.midup.common.MidupCommon;
import br.com.midup.api.data.FindResult.Result;
import br.com.midup.common.logging.FormattedLogger;
import br.com.midup.common.player.BasePlayer;
import br.com.midup.api.player.Group;
import br.com.midup.api.player.Role;

/**
 * Base class for handling input/output of player data from the database.
 *
 * TODO Very important! Find a way to fix the possible server data
 * synchronization issue. Do NOT delete this!
 *
 * @author WinX64
 *
 * @param <TPlayer>
 *            The Player implementation
 * @param <TData>
 *            The PlayerData implementation
 * @param <TRole>
 *            The Role implementation
 */
public abstract class BaseDataHandler<TPlayer extends BasePlayer<TData, TRole>, TData extends BasePlayerData, TRole extends Role> {

    private static final String CONFIG_FILE_NAME = "database.yml";
    protected static final String GLOBAL_COLLECTION_NAME = "global";
    protected static final UpdateOptions UPSERT = new UpdateOptions().upsert(true);

    protected final MidupCommon<TPlayer, TData, TRole> plugin;

    protected final File configFile;
    protected FileConfiguration config;
    protected final FormattedLogger logger;

    protected Map<UUID, TData> cachedPlayerDatas;
    protected final Document lookupFunction;

    protected MongoClient client;
    protected MongoDatabase database;
    protected MongoCollection<Document> globalCollection;
    protected MongoCollection<Document> specificCollection;

    private String address;
    private int port;
    private String username;
    private String password;
    private String databaseName;

    public BaseDataHandler(MidupCommon<TPlayer, TData, TRole> plugin) {
        this.plugin = plugin;
        this.configFile = new File(plugin.getDataFolder(), CONFIG_FILE_NAME);
        this.logger = new FormattedLogger(plugin.getParentLogger(), "Database");

        this.cachedPlayerDatas = new ConcurrentHashMap<>();
        this.lookupFunction = new Document("$lookup",
                new Document("from", plugin.getServerType().toString().toLowerCase()).append("localField", "_id")
                        .append("foreignField", "_id").append("as", plugin.getServerType().toString().toLowerCase()));

        this.address = "localhost";
        this.port = 27017;
        this.username = "root";
        this.password = "password";
        this.databaseName = "midup";
    }

    protected abstract void loadSpecificPlayerData(TData playerData, Document document);

    protected abstract void updateSpecificPlayerData(TData playerData);

    public abstract TData initializePlayerData(UUID uniqueId, String name);

    public final void loadConfiguration() {
        if (!configFile.exists()) {
            plugin.saveResource(CONFIG_FILE_NAME, true);
        }

        this.config = YamlConfiguration.loadConfiguration(configFile);
        if (config.contains("address")) {
            this.address = config.getString("address");
        } else {
            logger.log(Level.WARNING, "Missing 'address' from %s. Using default 'localhost'!", CONFIG_FILE_NAME);
        }
        if (config.contains("port")) {
            this.port = config.getInt("port");
        } else {
            logger.log(Level.WARNING, "Missing 'port' from %s. Using default '6696'!", CONFIG_FILE_NAME);
        }
        if (config.contains("username")) {
            this.username = config.getString("username");
        } else {
            logger.log(Level.WARNING, "Missing 'username' from %s. Using default 'root'!", CONFIG_FILE_NAME);
        }
        if (config.contains("password")) {
            this.password = config.getString("password");
        } else {
            logger.log(Level.WARNING, "Missing 'password' from %s. Using default 'password'!", CONFIG_FILE_NAME);
        }
        if (config.contains("database")) {
            this.databaseName = config.getString("database");
        } else {
            logger.log(Level.WARNING, "Missing 'database' from %s. Using default 'monkeycraft'", CONFIG_FILE_NAME);
        }
    }

    public final void openConnection() {
        ServerAddress serverAddress = new ServerAddress(address, port);
        MongoCredential credential = MongoCredential.createCredential(username, databaseName, password.toCharArray());
        MongoClientOptions options = MongoClientOptions.builder()
                .codecRegistry(CodecRegistries.fromRegistries(
                        CodecRegistries.fromCodecs(new UuidCodec(UuidRepresentation.STANDARD)),
                        MongoClient.getDefaultCodecRegistry()))
                .applicationName(plugin.getServerType().name()).connectionsPerHost(1).build();

        this.client = new MongoClient(serverAddress, Arrays.asList(credential), options);
        this.database = client.getDatabase(databaseName);
        this.globalCollection = database.getCollection(GLOBAL_COLLECTION_NAME);
        this.specificCollection = database.getCollection(plugin.getServerType().name().toLowerCase());

        this.globalCollection.createIndex(new Document("name", "text"));
    }

    public void cachePlayerData(UUID uniqueId, TData playerData) {
        this.cachedPlayerDatas.put(uniqueId, playerData);
    }

    public TData retrievePlayerData(UUID uniqueId) {
        return cachedPlayerDatas.remove(uniqueId);
    }

    public FindResult<TPlayer, TData, TRole> findDataByUniqueId(UUID uniqueId) {
        return findDataByUniqueId(uniqueId, DataField.all(), true);
    }

    public FindResult<TPlayer, TData, TRole> findDataByUniqueId(UUID uniqueId, DataField fields, boolean specificData) {
        return findData(new Document("$match", new Document("_id", uniqueId)), fields, specificData);
    }

    public FindResult<TPlayer, TData, TRole> findMultipleDataByUniqueId(List<UUID> uniqueIds) {
        return findMultipleDataByUniqueId(uniqueIds, DataField.all(), true);
    }

    public FindResult<TPlayer, TData, TRole> findMultipleDataByUniqueId(List<UUID> uniqueIds, DataField fields,
                                                                        boolean specificData) {
        List<Document> matches = new ArrayList<Document>();
        for (UUID uniqueId : uniqueIds) {
            matches.add(new Document("$match", new Document("_id", uniqueId)));
        }
        return findMultipleData(new Document("$or", matches), fields, specificData);
    }

    public FindResult<TPlayer, TData, TRole> findDataByName(String name) {
        return findDataByName(name, DataField.all(), true);
    }

    public FindResult<TPlayer, TData, TRole> findDataByName(String name, DataField fields, boolean specificData) {
        return findData(matchText(name, false), fields, specificData);
    }

    public FindResult<TPlayer, TData, TRole> findMultipleDataByName(List<String> names) {
        return findMultipleDataByName(names, DataField.all(), true);
    }

    public FindResult<TPlayer, TData, TRole> findMultipleDataByName(List<String> names, DataField fields,
                                                                    boolean specificData) {
        List<Document> matches = new ArrayList<Document>();
        for (String name : names) {
            matches.add(matchText(name, false));
        }
        return findMultipleData(new Document("$or", matches), fields, specificData);
    }

    private FindResult<TPlayer, TData, TRole> findData(Document match, DataField fields, boolean specificData) {
        MongoCursor<Document> cursor = null;
        try {
            cursor = this.globalCollection.aggregate(specificData
                    ? Arrays.asList(match, lookupFunction,
                    fields.project(plugin.getServerType().toString().toLowerCase()))
                    : Arrays.asList(match, fields.project())).batchSize(2).iterator();
            if (!cursor.hasNext()) {
                return new FindResult<>(Result.NOT_FOUND);
            }

            Document documentData = cursor.next();
            TData playerData = loadData(documentData, fields, specificData);

            return new FindResult<TPlayer, TData, TRole>(cursor.hasNext() ? Result.FOUND_MULTIPLE : Result.FOUND_ONE,
                    Collections.singletonList(playerData));
        } catch (Exception e) {
            logger.log(Level.SEVERE, e,
                    "An error occurred while finding the player data for match '%s'! Details below:", match.toJson());
            return new FindResult<TPlayer, TData, TRole>(Result.ERROR);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private FindResult<TPlayer, TData, TRole> findMultipleData(Document match, DataField fields, boolean specificData) {
        MongoCursor<Document> cursor = null;
        try {
            cursor = this.globalCollection.aggregate(specificData
                    ? Arrays.asList(match, lookupFunction,
                    fields.project(plugin.getServerType().toString().toLowerCase()))
                    : Arrays.asList(match, fields.project())).batchSize(10).iterator();
            List<TData> playersData = new ArrayList<TData>();

            while (cursor.hasNext()) {
                Document documentData = cursor.next();
                playersData.add(loadData(documentData, fields, specificData));
            }

            return new FindResult<TPlayer, TData, TRole>(playersData.isEmpty() ? Result.NOT_FOUND
                    : playersData.size() == 1 ? Result.FOUND_ONE : Result.FOUND_MULTIPLE, playersData);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e,
                    "An error occurred while finding the player datas for matches '%s'! Details below:",
                    match.toJson());
            return new FindResult<TPlayer, TData, TRole>(Result.ERROR);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private TData loadData(Document documentData, DataField fields, boolean includeSpecificData) {
        UUID uniqueId = documentData.get("_id", UUID.class);
        String name = fields.has(NAME) ? documentData.getString(NAME.getFieldName()) : null;

        TData playerData = this.initializePlayerData(uniqueId, name);

        if (fields.has(GROUP)) {
            int groupId = documentData.getInteger(GROUP.getFieldName());
            Group group = Group.getGroup(groupId);
            if (group == null) {
                throw new IllegalArgumentException("Invalid group id: " + groupId);
            }
            playerData.setGroup(group);
        }

        if (fields.has(GROUP_EXPIRY)) {
            playerData.setGroupExpiry(documentData.getInteger(GROUP_EXPIRY.getFieldName()));
        }

        if (fields.has(BANS)) {
            List<?> bansDocumentList = documentData.get(BANS.getFieldName(), List.class);
            for (Object banDocumentObject : bansDocumentList) {
                Document banDocument = (Document) banDocumentObject;
                UUID banningUniqueId = banDocument.get("banned_by", UUID.class);
                int banDate = banDocument.getInteger("ban_date");
                int expiry = banDocument.getInteger("expiry");
                String reason = banDocument.getString("reason");
                UUID unbanningUniqueId = banDocument.get("unbanned_by", UUID.class);
                int unbanDate = banDocument.getInteger("unban_date");
                playerData.getBans()
                        .add(new BanEntry(banningUniqueId, banDate, expiry, reason, unbanningUniqueId, unbanDate));

            }
        }

        if (fields.has(MUTES)) {
            List<?> mutesDocumentList = documentData.get(MUTES.getFieldName(), List.class);
            for (Object muteDocumentObject : mutesDocumentList) {
                Document muteDocument = (Document) muteDocumentObject;
                UUID mutingUniqueId = muteDocument.get("muted_by", UUID.class);
                int muteDate = muteDocument.getInteger("mute_date");
                int expiry = muteDocument.getInteger("expiry");
                String reason = muteDocument.getString("reason");
                UUID unmutingUniqueId = muteDocument.get("unmuted_by", UUID.class);
                int unmuteDate = muteDocument.getInteger("unmute_date");
                playerData.getMutes()
                        .add(new MuteEntry(mutingUniqueId, muteDate, expiry, reason, unmutingUniqueId, unmuteDate));

            }
        }

        if (includeSpecificData) {
            List<?> specificData = documentData.get(plugin.getServerType().toString().toLowerCase(), List.class);
            if (specificData.size() > 0) {
                this.loadSpecificPlayerData(playerData, (Document) specificData.get(0));
            }
        }

        return playerData;
    }

    public boolean updateData(TData dataHolder) {
        return updateData(dataHolder, DataField.all(), true);
    }

    public boolean updateData(TData dataHolder, DataField fields, boolean specificData) {
        try {
            Document filter = new Document("_id", dataHolder.getUniqueId());
            Document documentData = new Document();

            if (fields.has(NAME)) {
                documentData.append(NAME.getFieldName(), dataHolder.getName());
            }

            if (fields.has(GROUP)) {
                documentData.append(GROUP.getFieldName(), dataHolder.getGroup().getId());
            }

            if (fields.has(GROUP_EXPIRY)) {
                documentData.append(GROUP_EXPIRY.getFieldName(), dataHolder.getGroupExpiry());
            }

            if (fields.has(BANS)) {
                List<Document> bansDocumentList = new ArrayList<Document>();
                for (BanEntry entry : dataHolder.getBans()) {
                    Document banDocument = new Document();
                    banDocument.append("banned_by", entry.getBanningUniqueId());
                    banDocument.append("ban_date", entry.getBanDate());
                    banDocument.append("expiry", entry.getExpiry());
                    banDocument.append("reason", entry.getReason());
                    banDocument.append("unbanned_by", entry.getUnbanningUniqueId());
                    banDocument.append("unban_date", entry.getUnbanDate());
                    bansDocumentList.add(banDocument);
                }
                documentData.append(BANS.getFieldName(), bansDocumentList);
            }

            if (fields.has(MUTES)) {
                List<Document> mutesDocumentList = new ArrayList<Document>();
                for (MuteEntry entry : dataHolder.getMutes()) {
                    Document muteDocument = new Document();
                    muteDocument.append("muted_by", entry.getMutingUniqueId());
                    muteDocument.append("mute_date", entry.getMuteDate());
                    muteDocument.append("expiry", entry.getExpiry());
                    muteDocument.append("reason", entry.getReason());
                    muteDocument.append("unmuted_by", entry.getUnmutingUniqueId());
                    muteDocument.append("unmute_date", entry.getUnmuteDate());
                    mutesDocumentList.add(muteDocument);
                }
                documentData.append(MUTES.getFieldName(), mutesDocumentList);
            }

            this.globalCollection.updateOne(filter, new Document("$set", documentData), UPSERT);

            if (specificData) {
                this.updateSpecificPlayerData(dataHolder);
            }

            return true;
        } catch (Exception e) {
            logger.log(Level.SEVERE, e,
                    "An error occurred while updating the player data for unique-id '%s'! Details below:",
                    dataHolder.getUniqueId());
            return false;
        }
    }

    private Document matchText(String text, boolean caseSensitive) {
        return new Document("$match",
                new Document("$text", new Document("$search", text).append("$caseSensitive", caseSensitive)));
    }
}
