package br.com.midup.common.networking.packet.out;

import br.com.midup.common.networking.packet.PacketOut;
import br.com.midup.common.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataOutput;

import java.util.UUID;

public class PacketOutBanRequest extends PacketOut {

    private final UUID uniqueId;
    private final UUID banningUniqueId;
    private String banningName;
    private final String reason;
    private final int duration;

    public PacketOutBanRequest(UUID uniqueId, UUID banningUniqueId, String banningName, String reason, int duration) {
        super(PacketType.Out.BAN_REQUEST);
        this.uniqueId = uniqueId;
        this.banningUniqueId = banningUniqueId;
        this.banningName = banningName;
        this.reason = reason;
        this.duration = duration;
    }

    @Override
    public void writePacketData(ByteArrayDataOutput output) {
        output.writeLong(uniqueId.getMostSignificantBits());
        output.writeLong(uniqueId.getLeastSignificantBits());

        output.writeLong(banningUniqueId.getMostSignificantBits());
        output.writeLong(banningUniqueId.getLeastSignificantBits());

        output.writeUTF(banningName);
        output.writeUTF(reason);
        output.writeInt(duration);
    }
}