package br.com.midup.common.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public final class Reflection {

    private static final String VERSION = "net.minecraft.server.v1_7_R3.MinecraftServer".split("\\.")[3];

    private static final String NMS = "net.minecraft.server." + VERSION;
    private static final String OBC = "org.bukkit.craftbukkit." + VERSION;

    private Reflection() {}

    public static Class<?> getNMSClass(String name) {
        return getNMSClass(name, null);
    }

    public static Class<?> getNMSClass(String name, String nmsPackage) {
        try {
            String fullName = nmsPackage == null ? NMS + name : NMS + nmsPackage + "." + name;
            return Class.forName(fullName);
        } catch (Exception e) {
            return null;
        }
    }

    public static Class<?> getOBCClass(String name) {
        return getOBCClass(name, null);
    }

    public static Class<?> getOBCClass(String name, String obcPackage) {
        try {
            String fullName = obcPackage == null ? OBC + name : OBC + obcPackage + "." + name;
            return Class.forName(fullName);
        } catch (Exception e) {
            return null;
        }
    }

    public static Field getField(Class<?> theClass, String fieldName) {
        try {
            Field theField = theClass.getDeclaredField(fieldName);
            theField.setAccessible(true);
            return theField;
        } catch (Exception e) {
            return null;
        }
    }

    public static Method getMethod(Class<?> theClass, String methodName, Class<?> args) {
        try {
            Method theMethod = theClass.getDeclaredMethod(methodName, args);
            theMethod.setAccessible(true);
            return theMethod;
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) {
        System.out.println(VERSION);
    }
}